// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import './ERC721Enumerable.sol';
import './Strings.sol';


contract EDXN is ERC721Enumerable {
    using Strings for uint256;


    uint256 private constant startIDFrom=1;// The initial token ID
    uint256 public constant COUNT_PRESALE = 250;
    uint256 public  constant PUBLIC_MAX = 9999;
    uint256 public constant PURCHASE_LIMIT = 3;

    uint256 public saleMode = 0;//0-none, 1-presale, 2-public
    uint256 public PRICE_PRESALE = 0.05 ether;
    uint256 public PRICE_WHITE = 0.07 ether;
    uint256 public PRICE = 0.08 ether;


    bool public isActive = false;
    bool public isAllowListActive = false;
    string public proof;

    uint256 public allowListMaxMint = 3;

    uint256 public totalPublicSupply;
    uint256 public startDate;

    address[] internal _ownersList;

    mapping(address => bool) private _allowList;
    mapping(address => uint256) private _claimed;

    string private _contractURI = '';
    string private _tokenBaseURI = '';
    string private _tokenRevealedBaseURI = '';

    

    bytes32 public lastOperation;
    address public lastOwner;

    bool public revealed = false;
    string public notRevealedUri;

    constructor(string memory name, string memory symbol) ERC721(name, symbol) {
        _ownersList.push(_msgSender());
    }


 

    function updateOwnersList(address[] calldata addresses) external onlyTwoOwners {
        _ownersList = addresses;
    }

    function onOwnersList(address addr) external view returns (bool) {
        for(uint i = 0; i < _ownersList.length; i++) {
            if (_ownersList[i] == addr) {
                return true;
            }
        }
        return false;
    }

    function addToAllowList(address[] calldata addresses) external onlyOwner {
        for (uint256 i = 0; i < addresses.length; i++) {
            require(addresses[i] != address(0), "Can't add the null address");

            _allowList[addresses[i]] = true;
            _claimed[addresses[i]] > 0 ? _claimed[addresses[i]] : 0;
        }
    }

    function onAllowList(address addr) external view returns (bool) {
        return _allowList[addr];
    }

    function removeFromAllowList(address[] calldata addresses) external onlyOwner {
        for (uint256 i = 0; i < addresses.length; i++) {
            require(addresses[i] != address(0), "Can't add the null address");
            _allowList[addresses[i]] = false;
        }
    }

    function setPricePreSale(uint256 newPrice) external  onlyOwner {
        PRICE_PRESALE = newPrice;
    }
    function setPriceWhite(uint256 newPrice) external  onlyOwner {
        PRICE_WHITE = newPrice;
    }

    function setPrice(uint256 newPrice) external  onlyOwner {
        PRICE = newPrice;
    }


    function setStartDate(uint256 newDate) external  onlyOwner {
        startDate = newDate;
    }

    function claimedBy(address addr) external view  returns (uint256){
        require(addr != address(0), "Can't check the null address");
        return _claimed[addr];
    }

    function purchase(uint256 numberOfTokens) external  payable {
        require(block.timestamp > startDate, 'Sale not started');
        require(isActive, 'Contract is not active');
        require(saleMode>0, 'The sale mode is not enabled');
        require(numberOfTokens <= PURCHASE_LIMIT, 'Would exceed PURCHASE_LIMIT');

        if (isAllowListActive) {
            require(_allowList[msg.sender], 'You are not on the Allow List');
        }

        //Mechanics of price selection
        uint256 Price=PRICE;
        if (saleMode == 1) {
            Price=PRICE_PRESALE;
        }
        else
        if(_allowList[msg.sender]){
            Price=PRICE_WHITE;
        }

        //optimistic set
        uint256 tokenCount=totalPublicSupply;

        _claimed[msg.sender] += numberOfTokens;
        totalPublicSupply += numberOfTokens;

        //Mechanics of determining the end of the pre sale
        if (saleMode == 1) {
            require(totalPublicSupply <= COUNT_PRESALE, 'All PRESALE tokens have been minted');
            if(totalPublicSupply >= COUNT_PRESALE)
                saleMode=0;
        }

        require(_claimed[msg.sender] <= allowListMaxMint, 'Purchase exceeds max allowed');
        require(totalPublicSupply  <= PUBLIC_MAX, 'Purchase would exceed PUBLIC_MAX');
        require(Price * numberOfTokens <= msg.value, 'ETH amount is not sufficient');




        for (uint256 i = 0; i < numberOfTokens; i++) {

            uint256 tokenId = startIDFrom+tokenCount;
            tokenCount+=1;
            _safeMint(msg.sender, tokenId);

        }
    }


    function setIsActive(bool _isActive) external  onlyOwner {
        isActive = _isActive;
    }

    function setIsAllowListActive(bool _isAllowListActive) external  onlyOwner {
        isAllowListActive = _isAllowListActive;
    }
    function setSaleMode(uint256 _Mode) external onlyOwner {
        saleMode = _Mode;
    }



    function setAllowListMaxMint(uint256 maxMint) external  onlyOwner {
        allowListMaxMint = maxMint;
    }

    function setProof(string calldata proofString) external  onlyOwner {
        proof = proofString;
    }

    function withdraw() external  onlyTwoOwners {
        require(_ownersList.length > 0, "Can't withdraw where owners list empty");

        int256 balance=int256(address(this).balance);
        require(balance>0, "Can't withdraw - no funds available");
        

        uint256 part = uint256(balance) / _ownersList.length;
        if(part>0)
        {
            for(uint256 i = 0; i < _ownersList.length; i++) {
                payable(_ownersList[i]).transfer(part);
            }
        }
    }

    function setContractURI(string calldata URI) external  onlyOwner {
        _contractURI = URI;
    }

    function setBaseURI(string calldata URI) external  onlyOwner {
        _tokenBaseURI = URI;
    }

    function setRevealedBaseURI(string calldata revealedBaseURI) external  onlyOwner {
        _tokenRevealedBaseURI = revealedBaseURI;
    }

    function contractURI() public view  returns (string memory) {
        return _contractURI;
    }

    function tokenURI(uint256 tokenId) public view override(ERC721) returns (string memory) {
        require(_exists(tokenId), 'Token does not exist');

        if(revealed == false) {
            return notRevealedUri;
        }

        string memory revealedBaseURI = _tokenRevealedBaseURI;
        return bytes(revealedBaseURI).length > 0 ?
            string(abi.encodePacked(revealedBaseURI, tokenId.toString(), '.json')) :
            _tokenBaseURI;
    }

    function reveal() public onlyOwner {
        revealed = true;
    }
    function setNotRevealedURI(string memory _notRevealedURI) public onlyOwner {
        notRevealedUri = _notRevealedURI;
    }

 
   //---------------------------------------------------------------------------------------------------------MultSig 2/N

     // MODIFIERS

    /**
    * @dev Allows to perform method by any of the owners
    */
    modifier onlyOwner {

        require(isOwner(), "onlyOwner: caller is not the owner");
        
        _;

    }


    /**
    * @dev Allows to perform method only after many owners call it with the same arguments
    */
    modifier onlyTwoOwners {

        require(isOwner(), "onlyTwoOwners: caller is not the owner");

        bytes32 operation = keccak256(msg.data);

        if(_ownersList.length == 1 || (lastOperation == operation && lastOwner != msg.sender))
        {
            resetVote();
            _;
        }
        else
        if(lastOperation != operation || lastOwner == msg.sender)
        {
            //new vote
            lastOperation = operation;
            lastOwner = msg.sender;
        }

     }


    /**
     * @dev Returns the address of the current owner
     */
    function owner() public view virtual returns (address) {
        return _ownersList[0];
    }

    /**
     * @dev Returns a list of owners addresses
     */
    function owners() public view virtual returns (address [] memory) {
        return _ownersList;
    }
 

   function isOwner()internal view returns(bool) {

        for(uint256 i = 0; i < _ownersList.length; i++) 
        {
            if(_ownersList[i]==msg.sender)
            {
                return true;
            }
        }

        return false;
    }

    function resetVote()internal{

        lastOperation=0;
        lastOwner=address(0);
    }

    /**
    * @dev Allows owners to change their mind by cacnelling vote operations
    */
    function cancelVote() public onlyOwner {
        resetVote();
    }



}