// SPDX-License-Identifier: MIT
pragma solidity ^0.8.2;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";

//https://docs.google.com/document/d/1I01qfnqxXjCee8nqjmiWGsZy-Qp9WK-Tr4hPhbQR2e0/edit?usp=sharing


contract BlockStaking is AccessControl
{
    uint256 constant PRICE_PRECISION=1e18;

    uint256 public poolToken;
    mapping(address => uint256) private userToken;
    mapping(address => uint256) private userReward;
    mapping(address => uint256) private userTRate;

    IERC20 public smartToken;
    IERC20 public smartHGD;
    
    uint256 public blockReward;
    uint256 public blockStart;
    uint256 public blockPeriod;

    uint256 public TRate;
    uint256 public Time;

    bytes32 public constant FUND_ROLE = keccak256("FUND_ROLE");
 
    constructor(address _smartHGD,address _smartToken) 
    {
        smartHGD=IERC20(_smartHGD);
        smartToken=IERC20(_smartToken);

        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
        _grantRole(FUND_ROLE, msg.sender);
    }


    function RecalTRate() internal
    {
        uint256 Rate=0;

        //calc Rate and recal block reward
        if(blockStart>0 && blockPeriod>0 && poolToken>0)
        {
            
            uint256 RewardOneBlock=PRICE_PRECISION*blockReward/blockPeriod;
            uint256 Period;
            if(blockStart+blockPeriod>block.number)
            {
                Period=block.number-blockStart;
                blockPeriod-=Period;
                blockStart+=Period;
            }
            else
            {
                Period=blockPeriod;
                blockPeriod=0;
                blockStart=0;
            }

            uint256 Reward=RewardOneBlock*Period/PRICE_PRECISION;
            blockReward-=Reward;

            Rate=PRICE_PRECISION*Reward/poolToken;
        }

        //recalc TRate
        if(Time>0)
            TRate+=Rate*(block.number-Time);
        else
            TRate=Rate;
        Time=block.number;
       
    }

    //amount in HGD
    function addHGD(uint256 amount, uint256 _period) onlyRole(FUND_ROLE) public
    {
        require(blockStart+blockPeriod<block.number,"The previous period has not been completed yet");
        require(_period>0,"Error period");
        require(amount>0,"Error amount HGD");
        require(smartHGD.transferFrom(msg.sender, address(this), amount),"Error transfer HGD");

        RecalTRate();

        blockReward=amount;
        blockStart=block.number;
        blockPeriod=_period;
    }

    //amount in tokens (LP USDC, LP ETH...)
    function Stake(uint256 amount) public
    {
        require(amount>0,"Error amount tokens");
        require(smartToken.transferFrom(msg.sender, address(this), amount),"Error transfer tokens");

        RecalTRate();
        
        
        //Recal Withdraw HGD
        userReward[msg.sender] = GetUserReward();


        //Tokens
        poolToken+=amount;
        userToken[msg.sender]+=amount;
        userTRate[msg.sender]=TRate;

    }

    
    function Withdraw() public
    {
        WithdrawHGD();

        uint256 withdrawToken=userToken[msg.sender];
        userToken[msg.sender]=0;
        smartToken.transfer(msg.sender, withdrawToken);
    }


    function WithdrawHGD() public
    {
        uint256 amountToken=userToken[msg.sender];
        require(amountToken>0,"There are not enough tokens on the user's balance");

        RecalTRate();

        
        uint256 withdrawHGD=GetUserReward();

        userReward[msg.sender]=0;
        userTRate[msg.sender]=TRate;
        smartHGD.transfer(msg.sender, withdrawHGD);
    }




 
    function GetUserToken() public view returns (uint256)
    {
        return userToken[msg.sender];
    }

    function GetUserReward() internal view returns (uint256)
    {
        uint256 amountToken=userToken[msg.sender];

        return userReward[msg.sender] + (TRate-userTRate[msg.sender])*amountToken/PRICE_PRECISION;
    }

}

