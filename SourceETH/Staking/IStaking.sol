// SPDX-License-Identifier: MIT
pragma solidity ^0.8.2;



interface IStaking
{
    //amount in HGD
    function addHGD(uint256 amount) external;

    //amount in tokens (LP USDC, LP ETH...)
    function Stake(uint256 amount) external;

    //amount in percent of tokens (100% = 1e5)
    function Withdraw(uint256 percent) external;
    
    //amount in percent of tokens (100% = 1e5)
    function WithdrawHGD(uint256 percent) external;


 
    function GetUserToken() external view returns (uint256);
    function GetUserStock() external view returns (uint256);
    function GetUserReward() external view returns (uint256);
}

