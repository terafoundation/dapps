// SPDX-License-Identifier: MIT
pragma solidity ^0.8.2;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";




contract Staking
{
    uint256 constant PRICE_PRECISION=1e18;

    uint256 public poolReward;
    uint256 public poolToken;
    uint256 public poolStock;
    mapping(address => uint256) private userToken;
    mapping(address => uint256) private userStock;

    IERC20 public smartHGD;
    IERC20 public smartToken;
    
    


 
    constructor(address _smartHGD,address _smartToken) 
    {
        smartHGD=IERC20(_smartHGD);
        smartToken=IERC20(_smartToken);
    }
    
    //amount in HGD
    function addHGD(uint256 amount) public
    {
        require(amount>0,"Error amount HGD");

       require(smartHGD.transferFrom(msg.sender, address(this), amount),"Error transfer HGD");
       poolReward+=amount;
    }

    //amount in tokens (LP USDC, LP ETH...)
    function Stake(uint256 amount) public
    {
        require(amount>0,"Error amount tokens");
        require(smartToken.transferFrom(msg.sender, address(this), amount),"Error transfer tokens");

        //stock value
        uint256 amountStock;
        uint256 fundAmount=poolToken + poolReward;
        if(fundAmount!=0)
            amountStock=amount*poolStock/fundAmount;
        else
            amountStock=amount;

        poolStock+=amountStock;
        userStock[msg.sender]+=amountStock;

        //Tokens
        poolToken+=amount;
        userToken[msg.sender]+=amount;

    }

    //amount in percent of tokens (100% = 1e5)
    function Withdraw(uint256 percent) public
    {
        require(percent>0 && percent<=1e5,"Error percent amount");
        
        WithdrawHGDInner(percent,true);

        uint256 amountStock=userStock[msg.sender];
        uint256 amountToken=userToken[msg.sender];

        require(amountToken>0,"There are not enough tokens on the user's balance");

        //Stock
        uint256 withdrawStock=amountStock*percent/1e5;
        poolStock-=withdrawStock;
        userStock[msg.sender]-=withdrawStock;
        

        //Token
        uint256 withdrawToken=amountToken*percent/1e5;
        poolToken-=withdrawToken;
        userToken[msg.sender]-=withdrawToken;
        smartToken.transfer(msg.sender, withdrawToken);

    }

    //amount in percent (100% = 1e5)
    function WithdrawHGD(uint256 percent) public
    {
        WithdrawHGDInner(percent,false);
    }





    function WithdrawHGDInner(uint256 percent, bool Silent) internal
    {
        require(percent>0 && percent<=1e5,"Error percent amount");

 
        uint256 amountStock=userStock[msg.sender];
        uint256 amountToken=userToken[msg.sender];


        uint256 Price=GetPriceStock();
        uint256 fundToken=amountStock*Price/PRICE_PRECISION;
        

        if(fundToken<=amountToken)
        {
            if(Silent)
                return;
            require(false,"There are not enough HGD rewards for this user");
        }

        uint256 withdrawAmount=(fundToken-amountToken)*percent/1e5;
        uint256 fundTokenNew=fundToken-withdrawAmount;
       

        //new stock value
        uint256 amountStockNew;
        if(Price!=0)
            amountStockNew=PRICE_PRECISION*fundTokenNew/Price;
        else
            amountStockNew=fundTokenNew;


        poolReward-=withdrawAmount;
        poolStock-=amountStock-amountStockNew;
        userStock[msg.sender]=amountStockNew;

        smartHGD.transfer(msg.sender, withdrawAmount);
    }


    function GetPriceStock() public view returns (uint256 Result)
    {
        if(poolStock!=0)
            Result=PRICE_PRECISION*(poolToken + poolReward)/poolStock;
        else
            Result=0;
    }


 
    function GetUserToken() public view returns (uint256)
    {
        return userToken[msg.sender];
    }
    function GetUserStock() public view returns (uint256)
    {
        return userStock[msg.sender];
    }

    
    function GetUserReward() public view returns (uint256)
    {

        uint256 amountStock=userStock[msg.sender];
        uint256 amountToken=userToken[msg.sender];


        uint256 fundToken=amountStock*GetPriceStock()/PRICE_PRECISION;
        

        if(fundToken<=amountToken)
            return 0;

        uint256 withdrawAmount=(fundToken-amountToken);
        return withdrawAmount;
    }

}

