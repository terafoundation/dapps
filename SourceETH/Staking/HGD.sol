// SPDX-License-Identifier: MIT
pragma solidity ^0.8.2;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";


contract HGD is ERC20 {
    constructor() ERC20("HGD Token", "HGD") {
        _mint(msg.sender, 100*1e6 * 10 ** decimals());
    }
}
