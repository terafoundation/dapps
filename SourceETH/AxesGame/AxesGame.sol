// SPDX-License-Identifier: MIT

pragma solidity ^0.8.2;

import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";



contract _Test
{
    struct TypeRow
    {
        uint64 start;
        uint64 end;
        uint64 active;
        string str;
    }

    struct TypeRowNum
    {
        uint64 num;
        TypeRow data;
    }

    function addRow(TypeRow memory _row) public
    {

    }
    function writeRow(TypeRowNum memory _row) public
    {

    }
    
      
    function getRowArr(uint256 _fromNum, uint256 _count) public pure returns (TypeRowNum[] memory Arr)
    {
        Arr=new TypeRowNum[](_count);
        TypeRowNum memory row=Arr[_fromNum];
        row.num=1;
        row.data.start=100;
        row.data.end=200;
        row.data.active=1;
        row.data.str=string(abi.encodePacked("11111111111111111111111111111",",","2222222222222222222222222222222222222222222222222","333333333333333333333333333"));
        //row.data.str="11111111111111111111111111111_2222222222222222222222222222222222222222222222222_333333333333333333333333333";
        
    }
}

interface IAxesHub
{
    function register(string memory name,string memory data) external;
}


contract StakingHero
{
    //uint256[]internal List;
    uint256 constant PRICE_PRECISION=1e18;

    uint256 public poolToken;
    mapping(address => uint256) private userToken;
    mapping(address => uint256) private userReward;
    mapping(address => uint256) private userTRate;

    
    uint256 public blockReward;
    uint256 public blockStart;
    uint256 public blockPeriod;

    uint256 public TRate;
    uint256 public Time;

    IERC20 public smartReward;
    IERC721 public smartNFT;
    IAxesHub public smartHub;

    mapping(uint256 => address) private userNFT;



    constructor(address _smartReward,address _smartNFT, address _smartHub) 
    {
        smartReward=IERC20(_smartReward);
        smartNFT=IERC721(_smartNFT);
        smartHub=IAxesHub(_smartHub);

        //
    }

    function Stake(uint256 _id) public
    {
        /*
            проверка что этот NFT еще не получали
            трансфер NFT от игрока
            проверка получения
            добавление нового элемента в список
            регистрация события в AxesHub
        */
        require(userNFT[_id]==address(0),"NFT was received");

        smartNFT.safeTransferFrom(msg.sender, address(this), _id);

        require(userNFT[_id]==msg.sender,"NFT not was received");

        //List.push(_id);

        smartHub.register("Stake",string(abi.encode(_id)));
    }


    function Withdraw(uint256 _id) public
    {
        /*
            проверка, что игрок является собственником
            отправка NFT игроку + выплата профита если есть
            регистрация события в AxesHub
        */
        require(userNFT[_id]==msg.sender,"sender not NFT owner");

       
        WithdrawProfit();

        smartNFT.safeTransferFrom(address(this), msg.sender, _id);
        userNFT[_id]=address(0);

 
        smartHub.register("Withdraw",string(abi.encode(_id)));
    }
    
    function AddProfit(uint256 _amount,uint256 _period) public payable
    {
        _period;

        /*
            переиспользование смарта BlockStaking
        */

        require(_amount>0,"Error amount tokens");
        require(smartReward.transferFrom(msg.sender, address(this), _amount),"Error transfer tokens");
        
        //вызов методов смарта BlockStaking


        smartHub.register("addProfit",string(abi.encode(_amount,",",_period)));
    }

    function WithdrawProfit() public
    {
        /*
            переиспользование смарта BlockStaking
        */

        smartHub.register("WithdrawProfit","");
    }


/*
    function getCount() public view returns (uint256 count)
    {
        count=List.length;
    }
    function getRow(uint256 _num) public view returns (uint256 id)
    {
        id=List[_num];
    }
    function getRowArr(uint256 _fromNum, uint256 _count) public view returns (uint256[] memory Arr)
    {
        Arr=new uint256[](_count);
        for(uint256 i=0;i<_count;i++)
            Arr[i]=List[i+_fromNum];
    }
*/
    function onERC721Received(address operator,address from,uint256 tokenId,bytes calldata data) public returns (bytes4)
    {
        operator;
        data;

        userNFT[tokenId]=from;
        return bytes4(keccak256("onERC721Received(address,address,uint256,bytes)"));
    }
    
}



contract SummonOld
{
    IERC20 public smartToken;
    IERC721 public smartNFT;
    IAxesHub public smartHub;

    mapping(bytes32 => uint256) private priceList;
    mapping(bytes32 => uint256) private nftList;

    function AddSummonBunch(bytes32 _list, uint256 _price, uint256[] memory _Arr) public
    {
    /*
        проверка роли администратора
        установка нового или редактирование существующего списка NFT
        регистрация события в AxesHub
    */
        SetPrice(_list, _price);
        for(uint256 i=0;i<_Arr.length;i++)
            AddNFT(_list, _Arr[i]);

        smartHub.register("AddSummonBunch",string(abi.encode(_list,_Arr)));
    }

    function RemoveSummonBunch(bytes32 _list) public
    {
    /*
        проверка роли администратора
        удаление существующего списка NFT
        регистрация события в AxesHub
    */

        SetPrice(_list,0);

        smartHub.register("RemoveSummonBunch",string(abi.encode(_list)));
    }

    function SetPrice(bytes32 _list, uint256 _price) public
    {
    /*
        проверка роли администратора
        установка новой цены существующего списка
        регистрация события в AxesHub
    */
        bytes32 key=keccak256(abi.encodePacked(_list));
        priceList[key]=_price;

        smartHub.register("SetPrice",string(abi.encode(_list,_price)));
    }

    function GetPrice(bytes32 _list) public view returns(uint256)
    {
        bytes32 key=keccak256(abi.encodePacked(_list));
        return priceList[key];
    }

    function purchase(bytes32 _list, uint256 _id) public payable
    {
    /*
        проверка корректности цены
        прием монет
        удаление NFT из доступного списка
        отправка NFT пользователю
        регистрация события в AxesHub
    */
        GetPrice(_list);
        RemoveNFT(_list, _id);//todo
        smartNFT.safeTransferFrom(address(this), msg.sender, _id);

        smartHub.register("purchase",string(abi.encode(_list,_id)));
    }

    function AddNFT(bytes32 _list, uint256 _id) public
    {
    /*
        проверка роли администратора
        добавление одного NFT в список
        регистрация события в AxesHub
    */
        
        bytes32 key=keccak256(abi.encodePacked(_list,_id));
        nftList[key]=1;

        smartHub.register("AddNFT",string(abi.encode(_list,_id)));
    }

    function RemoveNFT(bytes32 _list, uint256 _id) public
    {
    /*
        проверка роли администратора
        удаление одного NFT из списка
        регистрация события в AxesHub
    */

        bytes32 key=keccak256(abi.encodePacked(_list,_id));
        nftList[key]=0;

        smartHub.register("RemoveNFT",string(abi.encode(_list,_id)));
    }
    
}

contract Tournaments
{
   mapping(bytes32 => bytes) private List;
}


contract LandingHero
{

    IERC20 public smartReward;
    IERC721 public smartNFT;
    IAxesHub public smartHub;

    mapping(uint256 => address) private userNFT;
    mapping(uint256 => uint256) private priceNFT;
    mapping(uint256 => uint256) private periodNFT;


    function AddToRent(uint256 _id, uint256 _price, uint256 _period) public
    {
    /*
        трансфер NFT от игрока
        добавление нового элемента в список userNFT[]  priceNFT[] periodNFT[]
        регистрация события в AxesHub
    */

        userNFT[_id]=msg.sender;
        priceNFT[_id]=_price;
        periodNFT[_id]=_period;
    }
    function Rent(uint256 _id) public
    {
        /*
            Трансфер оплаты на смарт
            Трансфер оплаты собственнику NFT
            Трансфер NFT игроку
        */
    }
 

    function withdraw(uint256 _id) public
    {
    /*
        проверка, что игрок является собственником
        если NFT не арендорвана отправка NFT игроку
        иначе если NFT арендорвана, но срок аренды прошел отправка NFT игроку принудительно
    */
    }




    function onERC721Received(address operator,address from,uint256 tokenId,bytes calldata data) public returns (bytes4)
    {
        operator;
        data;

        userNFT[tokenId]=from;
        return bytes4(keccak256("onERC721Received(address,address,uint256,bytes)"));
    }

}
