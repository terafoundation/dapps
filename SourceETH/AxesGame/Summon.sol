// SPDX-License-Identifier: MIT

pragma solidity ^0.8.2;
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";

contract IAxesHub
{
    function register(string memory name,string memory data) public
    {

    }
}

interface IAxes721
{
    function currentId() external view returns (uint _currentId);
    function systemMint(address to, string memory _item) external;
}


contract Summon is AccessControl
{
    bytes32 public constant ADMIN_ROLE = keccak256("ADMIN_ROLE");
    
    IERC20 public smartToken;
    IAxes721 public smartNFT;
    IAxesHub public smartHub;

    uint256 internal listCount;
    mapping(uint256 => bytes) private listMap;

    constructor(address _smartToken, address _smartNFT, address _smartHub)
    {
        smartToken=IERC20(_smartToken);
        smartNFT=IAxes721(_smartNFT);
        smartHub=IAxesHub(_smartHub);

        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
        _grantRole(ADMIN_ROLE, msg.sender);
    }
    

    struct SummonSet {
        
        bool       isActive;
        uint256    dateStart;
        uint256    dateStop;
        uint256    tokensPerBuyer;
        string     rndSet;
        uint256    Price;
    }    

    function addSummon(SummonSet memory item) onlyRole(ADMIN_ROLE) public
    {
        /*
            проверка роли администратора
            установка нового элемента списка

            регистрация события в AxesHub
        */
        listCount++;
        listMap[listCount]=abi.encode(item);

        smartHub.register("addSummon",string(abi.encode(item)));
    }

    function setSummon(uint256 _rndSetIndex, SummonSet memory item)  onlyRole(ADMIN_ROLE) public
    {
    /*
        проверка роли администратора
        редактирование существующего элемента

        регистрация события в AxesHub
    */
        require(_rndSetIndex>0 && _rndSetIndex<=listCount,"Error item index");

        listMap[_rndSetIndex]=abi.encode(item);

        smartHub.register("setSummon",string(abi.encode(_rndSetIndex,item)));
    }

    function editActive(uint256 _rndSetIndex, bool _Active)  onlyRole(ADMIN_ROLE) public
    {
    /*
        проверка роли администратора
        установка признака активности существующего элемента

        регистрация события в AxesHub
    */
    
        require(_rndSetIndex>0 && _rndSetIndex<=listCount,"Error item index");
        
        SummonSet memory item=getItem(_rndSetIndex);
        
        item.isActive=_Active;
        
        listMap[_rndSetIndex]=abi.encode(item);

        smartHub.register("editActive",string(abi.encode(_rndSetIndex,_Active)));
    }

   
 
    function purchase(uint256 _rndSetIndex, uint256 _tokenNumbers) public payable
    {
    /*
        проверка корректности цены, активности, дат
        прием монет
        минтинг через systemMint() и определение ее ID
        регистрация события в AxesHub
    */

        require(_rndSetIndex>0 && _rndSetIndex<=listCount,"Error item index");
        require(_tokenNumbers>0 && _tokenNumbers<1e6,"Error token numbers");
        
        SummonSet memory item=getItem(_rndSetIndex);
        uint256 allAmount=item.Price * _tokenNumbers;

        //прием монет
        require(smartToken.transferFrom(msg.sender, address(this), allAmount), "Error token transfer");


        for(uint256 i=0;i<_tokenNumbers;i++)
        {
            uint256 ID=smartNFT.currentId();
            smartNFT.systemMint(msg.sender,"");
            
            smartHub.register("purchase",string(abi.encode(_rndSetIndex,ID)));
        }
    }


    function withdraw(uint256 _amount) onlyRole(ADMIN_ROLE) public
    {
        //отправка монет
        smartToken.transfer(msg.sender, _amount);

        smartHub.register("withdraw",string(abi.encode(_amount)));
    }



    function getCount() public view returns(uint256)
    {
        return listCount;
    }

    function getList(uint256 _fromRndSetIndex, uint256 _count) public view returns (SummonSet[] memory Arr)
    {
    /*
        получение массива элементов начиная с заданного индекса _fromRndSetIndex, первый элемент начинается индекса 1
    */
            Arr=new SummonSet[](_count);
            for(uint256 i=0;i<_count;i++)
            {
                Arr[i]=getItem(i+_fromRndSetIndex);
            }

    }


    function getItem(uint256 _rndSetIndex)  public view returns (SummonSet memory)
    {
        bytes memory raw=listMap[_rndSetIndex];
        SummonSet memory item=abi.decode(raw,(SummonSet));

        return item;
    }

    function getItemRaw(uint256 _rndSetIndex)  public view returns (bytes memory Raw)
    {
        Raw=listMap[_rndSetIndex];
    }


}
