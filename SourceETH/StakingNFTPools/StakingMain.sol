// SPDX-License-Identifier: MIT
pragma solidity ^0.8.2;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";


interface StakingSet
{
}

contract StakingMain// is ERC721
{
    uint256 private listCount;
    mapping(uint256 => StakingSet) public listMap;
    mapping(uint256 => StakingSet) public smartByNFT;

    constructor()
    {
        //owner=msg.sender;
    }

    function Stake(uint256 _setNum) public payable
    {
        
        /*
            выбор смарта сета по номеру: smart=listMap[_setNum]
            создание прокси-адреса: smartAddress=new StakingAddress(smart);
            отправка монет на прокси-адрес: value=msg.value
            smart.Stake(smartAddress,value)
            mint NFT c ID=smartAddress
            запись в NFT смарта сета smartByNFT[_setNum]=smart
        */
        
    }
    function Withdraw(uint256 _id, uint256 _amount) public
    {
        /*
            проверка доступа: msg.sender==smartNFT.ownerOf(_id)
            определение смарта сета: smart=smartByNFT[_id]
            определение смарта прокси-адреса из _id
            smart.Withdraw(addr,_amount);
        */
    }

    function WithdrawReward(uint256 _id) public
    {
        /*
            проверка доступа: msg.sender==smartNFT.ownerOf(_id)
            определение смарта сета: smart=smartByNFT[_id]
            определение смарта прокси-адреса из _id
            smart.WithdrawReward(addr);
        */
    }

    function getNFTFields(uint256 _id)  public view returns (string memory)
    {
        /*
            определение смарта сета: smart=smartByNFT[_id]
            возврат описания сета
        */
    }




    function addSet(address _smart) public //only owner
    {
        //проверка доступа

        listCount++;
        listMap[listCount]=StakingSet(_smart);
    }

    function editSet(uint256 _setIndex, address _smart) public //only owner
    {
        //проверка доступа

        listMap[_setIndex]=StakingSet(_smart);
    }

    function getCount() public view returns(uint256)
    {
        return listCount;
    }

    function getList(uint256 _fromSetIndex, uint256 _count) public view returns (address[] memory Arr)
    {
    
        //получение массива элементов начиная с заданного индекса _fromSetIndex, первый элемент начинается индекса 1

        Arr=new address[](_count);
        for(uint256 i=0;i<_count;i++)
        {
            Arr[i]=getItem(i+_fromSetIndex);
        }

    }
    function getItem(uint256 _SetIndex)  public view returns (address)
    {
        return address(listMap[_SetIndex]);
    }

}

