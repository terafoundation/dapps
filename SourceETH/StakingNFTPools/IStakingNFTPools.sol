// SPDX-License-Identifier: MIT
pragma solidity ^0.8.2;

import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
//import "@openzeppelin/contracts/proxy/Proxy.sol";


interface StakingMain is IERC721
{
    /*
    HubRouting Hub;
    */


    function Stake(uint256 _setNum) external payable;
        /*
            mint NFT (get ID)
            Hub.Stake(_setNum,ID,value);
        */
        
    function Withdraw(uint256 _id, uint256 _amount) external;
        /*
            access control: msg.sender==ownerOf(_id)
            Hub.Withdraw(_id,_amount,msg.sender);
        */

    function WithdrawReward(uint256 _id) external;
        /*
            access control: msg.sender==ownerOf(_id)
            smart.WithdrawReward(_id,msg.sender);
        */

    function getNFTFields(uint256 _id)  external view returns (string memory);
        /*
            return of set description (list of pools and percentages) + number of coins
            return Hub.getNFTFields(_id);
        */




    function addSet(address _smart) external; //only owner
    /*
        adding a new smart set to the list
        access control
        Hub.RegistrationSet(_smart);
    */

    function editSet(uint256 _setIndex, address _smart) external; //only owner
    /*
        Changing the smart set
        access control
        Hub.UpdateSet(_setIndex, _smart);
    */

    function getCount() external view returns(uint256);
    /*
        return Hub.getCount();
    */

    function getList(uint256 _fromSetIndex, uint256 _count) external view returns (address[] memory Arr);
    /*
        getting an array of elements starting from the given index _fromSetIndex, the first element starts with index 1
        return Hub.getList(_fromSetIndex, _count);
    */


    function getItem(uint256 _SetIndex)  external view returns (address);
    /*
        return Hub.getList(_fromSetIndex);
    */

}

interface HubRouting
{
    /*
    StakingMain smartOwner;

    uint256 private listCount;
    mapping(uint256 => StakingSet) public listActive;
    mapping(uint256 => StakingSet) public listMap;
    mapping(uint256 => StakingSet) public smartByNFT;
    */

    
 

    function Stake(uint256 _setNum) external payable;
        /*
            access control from smartOwner
            selecting a smart set by number: smart=listMap[_setNum]
            sending coins to a smart + distribution by pools: smart.Stake(ID) //{value:msg.value, gas: 100000}
            write to NFT the smart set address: smartByNFT[ID]=smart
        */
        
    function Withdraw(uint256 _setNum,uint256 _id, uint256 _amount, address _to) external;
        /*
            access control from smartOwner
            selecting a smart set by number: smart=smartByNFT[_id]
            smart.Withdraw(_id,_amount,_to);
        */

    function WithdrawReward(uint256 _setNum,uint256 _id, address _to) external;
        /*
            access control from smartOwner
            selecting a smart set by number: smart=smartByNFT[_id]
            smart.WithdrawReward(_id,_to);
        */

    function getNFTFields(uint256 _id)  external view returns (string memory);
        /*
            return of set description (list of pools and percentages) + number of coins
            selecting a smart set by number: smart=smartByNFT[_id]
            return smart.getNFTFields(_id);
        */

    function getSetDescription(uint256 _setNum)  external view returns (string memory);
        /*
            return of set description
            selecting a smart set by number: smart=listMap[_setNum]
            return smart.getSetDescription();
        */



    
    function RegistrationSet(address _smart) external; //only owner
    /*
        access control from smartOwner
        adding a new smart set to the list

        listCount++;
        listMap[listCount]=HubRouting(_smart);
    */

    function UpdateSet(uint256 _setIndex, address _smart) external; //only owner
    /*
        access control from smartOwner
        Changing the smart set

        listMap[_setIndex]=HubRouting(_smart);
    */

    function DeactiveSet(uint256 _setNum) external;
    /*
        access control
        listActive[_setNum]=false;
    */
    function ActiveSet(uint256 _setNum) external;
    /*
        access control
        listActive[_setNum]=true;
    */
    function CallStakingSet(uint256 _setNum,uint256 _vote) external;
    /*
        access control
        some voting
    */



    function getCount() external view returns(uint256);
    /*
        return listCount;
    */

    function getList(uint256 _fromSetIndex, uint256 _count) external view returns (address[] memory Arr);
    /*
    
        getting an array of elements starting from the given index _fromSetIndex, the first element starts with index 1

        Arr=new address[](_count);
        for(uint256 i=0;i<_count;i++)
        {
            Arr[i]=getItem(i+_fromSetIndex);
        }
    */

    function getItem(uint256 _SetIndex)  external view returns (address);
    /*
        return address(listMap[_SetIndex]);
    */



}



interface StakingSetProxy// is Proxy
{
    function SetUpgrade(address _address)external;
   
}



interface StakingSet
{
    /*
     StakingMain smartOwner;

    uint256 public poolToken;
    mapping(address => uint256) private userToken;

    By the number of pools::
        IPool Pool;
        uint256 public poolReward;
        mapping(address => uint256) private userPass;
    
    function WithdrawRewardAll() internal
    {
        pool cycle
            de-awarding: value=pool.WithdrawReward();
            replenishing the overall award pool: poolReward+=value;
    }
    */
 

    function Stake(uint256 _id) external payable;
        /*
            uint256 _amount=msg.value;
            WithdrawRewardAll();
            access control from smartOwner
            writing the stacking amount to _id NFT
            pool cycle
                pool share calculation value=_amount*Percent
                buying pool tokens via Swap: value2
                steaking: pool.Stake(value2);
                calculate the unpaid remuneration: passValue =  value*poolReward/poolToken
                poolReward+=passValue;
                userPass[]=passValue;

            increase the coin pool: poolToken+=_amount
        */



    function Withdraw(uint256 _id, uint256 _amount, address _to) external;
         /*
            access control from smartOwner
            de-awarding: WithdrawReward();
            pool cycle
                pool share calculation value=_amount*Percent
                calculate award pool reduction: Pass=Percent*_amount/userToken[]*userPass[];
                decrease: userPass[]-=Pass и poolReward-=Pass;
                steak removal: pool.Withdraw(value);
                purchase of BNB, if required
                sending coins on _to
            
            decrease the coin pool: poolToken-=_amount
            writing the remaining amount of the steking to NFT _id 
        */




    function WithdrawReward(uint256 _id, address _to) external;
        /*
            WithdrawRewardAll();
            access control from smartOwner
            pool cycle
                calculate the reward: value =  userToken[]*poolReward/poolToken
                calculate the payout: pay=value-userPass[];
                retain payments made: userPass[]=value;
                sending coins on _to

        */




    function getNFTFields(uint256 _id)  external view returns (string memory);
        /*
            return of set description (list of pools and percentages) + number of coins
        */
    
    function getSetDescription()  external view returns (string memory);
        /*
            return of set description
        */
    
  
    

}
