// SPDX-License-Identifier: MIT
pragma solidity ^0.8.2;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";

interface StakingMain is IERC721
{

}

contract StakingAddress
{
    StakingSet smartSet;

    constructor(StakingSet _smartSet)
    {
        smartSet=_smartSet;
    }

    function Stake(address _pool, uint256 _amount) public
    {
        /*
            проверка доступа из smartSet
            отправка монет на IPool(_pool).Stake(_amount);
        */
    }    

    function Withdraw(address _pool, uint256 _amount, address payable _to) public
    {
        /*
            проверка доступа из smartSet
            снятие награды: WithdrawReward(_pool)
            снятие стейка: pool.Withdraw(_amount);
            отправка монет
        */
    }    

   function WithdrawReward(address _pool, address payable _to) public
    {
        /*
            проверка доступа из smartSet
            снятие награды: pool.WithdrawReward();
            отправка монет
         */
    }    

    receive() external payable
    {
        //прием Eth
    }

}


contract StakingSet
{
    address constant Pool1=0x5B38Da6a701c568545dCfcB03FcB875f56beddC4;
    address constant Pool2=0xAb8483F64d9C6d1EcF9b849Ae677dD3315835cb2;

    StakingMain smartNFT;
 
    constructor(StakingMain _smartNFT)
    {
        smartNFT=_smartNFT;
    }

    function Stake(StakingAddress smart, uint256 _amount) public
    {

        //StakingAddress smart=new StakingAddress();
        /*
            проверка доступа из smartNFT
            цикл по пулам
                стейкинг от имени прокси-адреса smart.Stake(Pool1, _amount*Percent);

        */

        smart.Stake(Pool1, _amount*30/100);
        smart.Stake(Pool2, _amount*70/100);

        
    }

    function Withdraw(StakingAddress smart, uint256 _amount) public
    {
        address payable to;

         /*
            проверка доступа из smartNFT
            определение адреса пользователя по ID NFT: to=smartNFT.ownerOf(to_uint(smart))
            цикл по пулам
                снятие стейка от имени прокси-адреса smart.Withdraw(Pool1, _amount*Percent, to);
        */

        smart.Withdraw(Pool1, _amount*30/100, to);
        smart.Withdraw(Pool2, _amount*70/100, to);

    }

    function WithdrawReward(StakingAddress smart) public
    {
        address payable to;
        /*
            проверка доступа из smartNFT
            определение адреса пользователя по ID NFT: to=smartNFT.ownerOf(to_uint(smart))
            цикл по пулам
                снятие награды от имени прокси-адреса smart.WithdrawReward(Pool1);
        */

        smart.WithdrawReward(Pool1, to);
        smart.WithdrawReward(Pool2, to);
   }



    function getNFTFields()  public view returns (string memory)
    {
        /*
            возврат описания сета
        */
    }

}
