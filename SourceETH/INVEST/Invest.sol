// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

interface IERC20 {

    function balanceOf(address who) external view returns (uint256);

    function transfer(address to, uint256 value) external returns (bool);


    function transferFrom(
        address from,
        address to,
        uint256 value
    ) external returns (bool);


}


contract Invest {
    
    IERC20 public smartTokenBUSD;//Binance-Peg BUSD Token (BUSD) 0xe9e7cea3dedca5984780bafc599bd69add087d56
    IERC20 public smartTokenUSDT;//Binance-Peg BSC-USD 0x55d398326f99059ff775485246999027b3197955
    IERC20 public smartToken1;
    IERC20 public smartToken2;
    uint256 public constant MODE_BUSD = 0;
    uint256 public constant MODE_USDT = 1;

    address public _owner;
    mapping(address => bool) internal mapReffs;

    uint256 constant PRICE_PRECISION=1e18;
    uint256 public Price;
    uint256 public RefPercent;//100% = 100 000


    constructor(address addressBUSD,address addressUSDT,address addressToken1,address addressToken2)
    {
        smartTokenBUSD=IERC20(addressBUSD);
        smartTokenUSDT=IERC20(addressUSDT);
        smartToken1=IERC20(addressToken1);
        smartToken2=IERC20(addressToken2);

        Price=0.003*1e18;
        RefPercent=5000;
        _owner=msg.sender;
    }

    modifier OnlyOwner()
    {
        require(msg.sender == _owner,"Need only owner access!!");
        _;
    }

    function tokenBalance1() public view returns (uint256)
    {
        return smartToken1.balanceOf(address(this));
    }
    function tokenBalance2() public view returns (uint256)
    {
        return smartToken2.balanceOf(address(this));
    }
    function moneyBalance(uint256 currency) public view returns (uint256)
    {
        require(currency<=MODE_USDT,"Error currency id");

        if(currency==MODE_BUSD)
            return smartTokenBUSD.balanceOf(address(this));
        else
            return smartTokenUSDT.balanceOf(address(this));
            
    }

    
    //----------------------------------------------------------------------------------
    function purchase(uint256 numberOfTokens, uint256 currency, address  addressRef) external
    {
        require(Price>0,"Price is zero");
        require(numberOfTokens>0 
                && numberOfTokens<=smartToken1.balanceOf(address(this))
                && numberOfTokens/10<=smartToken2.balanceOf(address(this))
                ,"Error tokens count");
        require(mapReffs[addressRef],"The addressRef is not registered");
        require(currency<=MODE_USDT,"Error currency id");
        
        

        IERC20 smartUSD;
        if(currency==MODE_BUSD)
            smartUSD = smartTokenBUSD;
        else
            smartUSD = smartTokenUSDT;

        //Get USD from client
        uint256 Amount = Price*numberOfTokens/PRICE_PRECISION;
        require(smartUSD.transferFrom(msg.sender, address(this), Amount),"Error transfer client USD");
        
        //Move tokens
        require(smartToken1.transfer(msg.sender, numberOfTokens),"Error transfer Token1");
        require(smartToken2.transfer(msg.sender, numberOfTokens/10),"Error transfer Token2");

        //Send RefPercent USD to Ref
        require(smartUSD.transfer(addressRef, RefPercent*Amount/100000),"Error transfer refs USD");

    }
    //----------------------------------------------------------------------------------



    function isRef(address addressRef) external view returns (bool)
    {
        return mapReffs[addressRef];
    }


    //---------------------------------------------------------------------------------- onlyOwner
    
    function SetOwner(address newOwner) external OnlyOwner
    {
        if (newOwner != address(0))
        {
            _owner = newOwner;
        }
    }

    function setPrice(uint256 NewPrice) external OnlyOwner
    {
        Price=NewPrice;
    }
    function setRefPercent(uint256 NewPercent) external OnlyOwner
    {
        RefPercent=NewPercent;
    }

    function registrationRef(address addressRef) external OnlyOwner
    {
        require(addressRef!=address(0),"Error address");
        require(!mapReffs[addressRef],"The address is already registered");

        mapReffs[addressRef]=true;
    }
    function deleteRegistrationRef(address addressRef) public OnlyOwner
    {
        require(addressRef!=address(0),"Error address");
        require(mapReffs[addressRef],"The address is not registered");

        mapReffs[addressRef]=false;
    }

    function withdrawMoney(uint currency) external OnlyOwner
    {
        uint256 amount=moneyBalance(currency);

        if(currency==MODE_BUSD)
            smartTokenBUSD.transfer(msg.sender, amount);
        else
            smartTokenUSDT.transfer(msg.sender, amount);
    }
    function withdrawToken1() external OnlyOwner
    {
        uint256 amount=tokenBalance1();
        smartToken1.transfer(msg.sender, amount);
    }
    function withdrawToken2() external OnlyOwner
    {
        uint256 amount=tokenBalance2();
        smartToken2.transfer(msg.sender, amount);
    }
    
}

