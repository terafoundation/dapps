﻿//Smart-contract: GOLD


// global context:
// context.BlockNum - Block num
// context.TrNum - Tx num
// context.Account - current account {Num,Currency,Smart,Value:{SumCOIN,SumCENT}}
// context.Smart - current smart {Num,Name,Account,Owner}
// context.FromNum - sender account num
// context.ToNum - payee account num
// context.Description - text
// context.Value - {SumCOIN,SumCENT}
// context.Currency - token currency
// context.ID - token ID
// context.SmartMode - 1 if run from smartcontract
// context.Tx - tx (not present in events)


"public"
function DappSale(Params)
{
    var Account=context.FromNum;
    if(!Account || typeof Account!=="number")
        throw "Error Account number";
        
    if(context.Smart.Account!==context.Account.Num)
        throw "Error context Account="+context.Account.Num;
        
    var Info=GetInfo();
    
    if(context.Currency!==Info.Currency)
        throw "Error currency value";
    
    if(ISZERO(context.Value))
        throw "Got zero value";
    
        
    var Price=Info.Price;
    Price=Price*(1+Info.PercentSale/100);
    
    var Value2=COIN_FROM_FLOAT(Avg(FLOAT_FROM_COIN(context.Value)/Price));
    ADD(Info.Pool1,context.Value);
    ADD(Info.Pool2,Value2);
    
    
    Mint(Account,"",Value2);
    Mint(0,"",Value2);
    
    
    
    WriteInfo(Info);
    
}


"public"
function DappBuy(Params)
{
    var Account=context.FromNum;
    if(!Account || typeof Account!=="number")
        throw "Error Account number";
        
    if(context.Smart.Account!==context.Account.Num)
        throw "Error context Account="+context.Account.Num;
        
    var Info=GetInfo();
    
    if(context.Currency!==context.Smart.Num)
        throw "Error currency value";
    
    if(ISZERO(context.Value))
        throw "Got zero value";
    
        
    var Price=Info.Price;
    var Sum1=Avg(FLOAT_FROM_COIN(context.Value)*(1-Info.PercentBuy/100)*Price);
    var Value1=COIN_FROM_FLOAT(Sum1);
    
    if(!SUB(Info.Pool2,context.Value))
        throw "Error Pool2 amount";
    if(ISZERO(Info.Pool2))
    {
        Value1=Info.Pool1;
        Info.Pool1={SumCOIN:0,SumCENT:0};
    }
    else
    {
        if(!SUB(Info.Pool1,Value1))
            throw "Error Pool1 amount";
    }
    
    
    
    Burn(context.Account.Num,"",context.Value);
    Burn(0,"",context.Value);
    WriteInfo(Info);
    
    Send(Account,Value1,"",Info.Currency,"");
    
}


"public"
function GetInfo()
{
    var Info=ReadInfo();
    // var Pool1=FLOAT_FROM_COIN(OnGetBalance(0,""));
    // var Pool2=FLOAT_FROM_COIN(context.Account.Value);
    var Pool1=FLOAT_FROM_COIN(Info.Pool1);
    var Pool2=FLOAT_FROM_COIN(Info.Pool2);
    Info.Price=Avg(Pool2?(Pool1/Pool2):Info.InitPrice);

    return Info;
}

function Avg(Sum)
{
    return Math.floor(Sum*1e9)/1e9;
}


function ReadInfo()
{
    var Item=ReadValue("POOLS");
    if(!Item)
        Item={Pool1:{SumCOIN:0,SumCENT:0} ,Pool2:{SumCOIN:0,SumCENT:0}, InitPrice:1,PercentBuy:5,PercentSale:5,Currency:0};

    return Item;
}

function WriteInfo(Item)
{
    WriteValue("POOLS",Item);
}

"public"
function InPlaceShow()
{
    return 1;
}


"public"
function OnTransfer(Params)
{
    //Params: From,To,Amount,ID
    if(!FLOAT_FROM_COIN(Params.Amount))
        return;

    if(Params.ID && (Params.Amount.SumCENT || typeof Params.Amount.SumCENT!=="number"))
        throw "Error Amount for NFT. Fractional values are prohibited.";

    if(Params.To<8)//burn
    {
        Burn(Params.From,Params.ID,Params.Amount);
        Burn(0,Params.ID,Params.Amount);
        return;
    }



    TransferGet(Params);
    return TransferSend(Params);
}

function TransferGet(Params)
{
    var Item1=ReadItem(Params.From);
    var Row1=FindRow(Item1.Arr,Params.ID);

    if(!SUB(Row1,Params.Amount))
        throw "There are not enough funds on the account: "+Params.From;
    if(ISZERO(Row1))
        DeleteRow(Item1.Arr,Params.ID);
    WriteItem(Item1);
}

function TransferSend(Params)
{
    var Item2=ReadItem(Params.To);
    var Row2=FindRow(Item2.Arr,Params.ID);
    ADD(Row2,Params.Amount);

    WriteItem(Item2);

    RegInWallet(Params.To);
}




"public"//for use from Proxy
function OnGetBalance(Account,ID)
{
    var Item=ReadItem(Account);
    if(!Item)
        Item={Arr:[]};
    if(ID!==undefined)
        return FindRow(Item.Arr,ID);

    var Arr=Item.Arr;
    for(var i=0;i<Arr.length;i++)
        if(Arr[i].ID)
            Arr[i].URI="/nft/"+Arr[i].ID;

    return Arr;
}



//Token Lib

function Format()
{
    return {Arr:[{ID:"str",SumCOIN:"uint",SumCENT:"uint32"}]};
}
function ReadItem(Account)
{
    var Key="ACC:"+Account;
    var Item=ReadValue(Key,Format());
    if(!Item)
        Item={Arr:[]};
    Item.Key=Key;
    return Item;
}

function WriteItem(Item)
{
    WriteValue(Item.Key,Item,Format());
}

function FindRow(Arr,ID)
{
    if(!ID)
        ID="";
    for(var i=0;i<Arr.length;i++)
        if(Arr[i].ID==ID)
        {
            return Arr[i];
        }

    var Row={ID:ID,SumCOIN:0,SumCENT:0};
    Arr.push(Row);
    return Row;
}

function DeleteRow(Arr,ID)
{
    if(!ID)
        ID="";
    for(var i=0;i<Arr.length;i++)
        if(Arr[i].ID==ID)
        {
            Arr.splice(i,1);
            break;
        }
}


function Mint(Account,ID,Amount)
{
    if(typeof Amount==="number")
        Amount=COIN_FROM_FLOAT(Amount);

    var Item=ReadItem(Account);
    var Row=FindRow(Item.Arr,ID);
    ADD(Row,Amount);
    WriteItem(Item);

    if(Account)
        RegInWallet(Account);
}

function Burn(Account,ID,Amount)
{
    if(typeof Amount==="number")
        Amount=COIN_FROM_FLOAT(Amount);

    var Item=ReadItem(Account);
    var Row=FindRow(Item.Arr,ID);
    if(!SUB(Row,Amount))
        throw "Error burn Amount in account: "+Account+" Lack of funds: "+(-FLOAT_FROM_COIN(Row));
    if(ISZERO(Row))
        DeleteRow(Item.Arr,ID);
    WriteItem(Item);
}




// //Owner mode

// "public"
// function DoMint(Params)
// {
//     CheckOwnerPermission();

//     Mint(Params.Account,Params.ID,Params.Amount);
//     Mint(0,Params.ID,Params.Amount);
// }

// "public"
// function DoBurn(Params)
// {
//     CheckOwnerPermission();
//     Burn(Params.Account,Params.ID,Params.Amount);
//     Burn(0,Params.ID,Params.Amount);
// }

// "public"
// function MintNFT(Params)
// {
//     CheckOwnerPermission();

//     Params.Amount=1;
//     Params.ID=""+(context.BlockNum*1000+context.TrNum);//self
//     DoMint(Params);
// }




// function CheckOwnerPermission()
// {
//     if(context.Smart.Num<7)//test mode
//         return;

//     if(context.FromNum!==context.Smart.Owner)
//         throw "Access is only allowed from Owner account";
// }


//static mode


"public"
function DoGetBalance(Params)
{
    return GetBalance(Params.Account,Params.Currency,Params.ID);
}


"public"
function TotalAmount(Params)
{
    return GetBalance(0,Params.Currency,Params.ID);
}



