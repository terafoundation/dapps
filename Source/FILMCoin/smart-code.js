﻿//Smart-contract: FILMCode


// global context:
// context.BlockNum - Block num
// context.TrNum - Tx num
// context.Account - current account {Num,Currency,Smart,Value:{SumCOIN,SumCENT}}
// context.Smart - current smart {Num,Name,Account,Owner}
// context.FromNum - sender account num
// context.ToNum - payee account num
// context.Description - text
// context.Value - {SumCOIN,SumCENT}
// context.Currency - token currency
// context.ID - token ID
// context.SmartMode - 1 if run from smartcontract
// context.Tx - tx (not present in events)








function GetBalanceRow(Items,ID)
{
    var Row=Items.Map[ID];
    if(!Row)
    {
        Row={ID:ID,SumCOIN:0,SumCENT:0, BlockTime:context.BlockNum};
        Items.Map[ID]=Row;
        Items.Arr.push(Row);
    }
        
    Row.Amount=FLOAT_FROM_COIN(Row);
    return Row;
}


function MapFromArr(Arr)
{
    var Map={};
    for(var i=0;Arr && i<Arr.length;i++)
    {
        var Item=Arr[i];
        Map[Item.ID]=Item;
    }
    return Map;
}








function AddBlockTime(Row,NewValue)
{
    var Sum1=FLOAT_FROM_COIN(Row);
    if(!Row.BlockTime || !Sum1)
        Row.BlockTime=context.BlockNum;
    else
    {
        var Sum2=FLOAT_FROM_COIN(NewValue);
        Row.BlockTime = (Sum1*Row.BlockTime + Sum2*context.BlockNum)/(Sum1+Sum2);
    }
}



//-------------------------------------------------------------------------------------------------------------INFO
"public"
function ReadInfo()
{
    var Item=ReadValue("INFO");
    if(!Item)
        Item={Map:{}};
    return Item;
}

function WriteInfo(Item)
{
    WriteValue("INFO",Item);
}

"public"
function SetInfo(Params)
{
    CheckOwnerPermission();
    WriteInfo(Params);
}




//-------------------------------------------------------------------------------------------------------------Token Lib
function Format()
{
    return {Arr:[{ID:"str",SumCOIN:"uint",SumCENT:"uint32", BlockTime:"double",    TimeValue:"double", Type:"str",Status:"byte", ContentID:"uint",Reserve:"str13"}]};
}


"public"
function InPlaceShow(Mode)
{
    return 1;
}


"public"
function OnTransfer(Params)
{
    //Params: From,To,Amount,ID
    if(!FLOAT_FROM_COIN(Params.Amount))
        return;

    if(Params.ID && (Params.Amount.SumCENT || typeof Params.Amount.SumCENT!=="number"))
        throw "Error Amount for NFT. Fractional values are prohibited.";

    if(Params.To<8)//burn
    {
        Burn(Params.From,Params.ID,Params.Amount);
        Burn(0,Params.ID,Params.Amount);
        return;
    }


    var Item1=ReadItem(Params.From);
    var Row1=FindRow(Item1.Arr,Params.ID);

    if(!SUB(Row1,Params.Amount))
        throw "There are not enough funds on the account: "+Params.From;
    if(ISZERO(Row1))
        DeleteRow(Item1.Arr,Params.ID);
    WriteItem(Item1);


    var Item2=ReadItem(Params.To);
    var Row2=FindRow(Item2.Arr,Params.ID);
    ADD(Row2,Params.Amount);
    Row2.ContentID=Row1.ContentID;
    Row2.Type=Row1.Type;
    Row2.Status=Row1.Status;

    AddBlockTime(Row2,Params.Amount);
    
    WriteItem(Item2);

    RegInWallet(Params.To);
}

"public"
function OnGetBalance(Account,ID)
{
    var Item=ReadItem(Account);
    if(!Item)
        Item={Arr:[]};
    if(ID!==undefined)
        return FindRow(Item.Arr,ID);

    var Arr=Item.Arr;
    for(var i=0;i<Arr.length;i++)
        if(Arr[i].ID)
            Arr[i].URI="/nft/"+Arr[i].ID;

    return Arr;
}


function ReadItem(Account)
{
    var Key="ACC:"+Account;
    var Item=ReadValue(Key,Format());
    if(!Item)
        Item={Arr:[]};
    Item.Key=Key;
    return Item;
}

function WriteItem(Item)
{
    WriteValue(Item.Key,Item,Format());
}

function FindRow(Arr,ID)
{
    if(!ID)
        ID="";
    for(var i=0;i<Arr.length;i++)
    if(Arr[i].ID==ID)
    {
        return Arr[i];
    }

    var Row={ID:ID,SumCOIN:0,SumCENT:0,BlockTime:0};
    Arr.push(Row);
    return Row;
}

function DeleteRow(Arr,ID)
{
    if(!ID)
        ID="";
    for(var i=0;i<Arr.length;i++)
    if(Arr[i].ID==ID)
    {
        Arr.splice(i,1);
        break;
    }
}


function Mint(Account,ID,Amount,Type,Status,ContentID)
{
    if(typeof Amount==="number")
        Amount=COIN_FROM_FLOAT(Amount);

    var Item=ReadItem(Account);
    var Row=FindRow(Item.Arr,ID);
    if(Type)
    {
        Row.Type=Type;
        Row.Status=Status;
    }
    if(ContentID)
        Row.ContentID=ContentID;
        
    ADD(Row,Amount);
    
    
    
    AddBlockTime(Row,Amount);
    WriteItem(Item);

    if(Account)
        RegInWallet(Account);
        
    
}

function Burn(Account,ID,Amount)
{
    if(typeof Amount==="number")
        Amount=COIN_FROM_FLOAT(Amount);

    var Item=ReadItem(Account);
    var Row=FindRow(Item.Arr,ID);
    if(!SUB(Row,Amount))
    {
        if(!Account)
            return;
            
        throw "Error burn Amount in account: "+Account+" Lack of funds: "+(-FLOAT_FROM_COIN(Row));
    }
    
    if(ISZERO(Row))
        DeleteRow(Item.Arr,ID);
    WriteItem(Item);
}

"public"
function MintNFT(Params)
{
    CheckOwnerPermission();

    Params.Amount=1;
    Params.ID=""+(context.BlockNum*1000+context.TrNum);//self
    DoMint(Params);
}




//Owner mode

"public"
function DoMint(Params)
{
    CheckOwnerPermission();

    Mint(Params.Account,Params.ID,Params.Amount,Params.Type,Params.Status,Params.ContentID);
    Mint(0,Params.ID,Params.Amount);
}

"public"
function DoBurn(Params)
{
    CheckOwnerPermission();
    Burn(Params.Account,Params.ID,Params.Amount);
    Burn(0,Params.ID,Params.Amount);
}

function CheckOwnerPermission()
{
    if(context.FromNum!==context.Smart.Owner)
        throw "Access is only allowed from Owner account";
}

// function CheckSenderPermission()
// {
//     if(context.FromNum!==context.Account.Num)
//         throw "Access is only allowed from Sender account: "+context.FromNum+"/"+context.Account.Num;
// }


//static mode


"public"
function DoGetBalance(Params)
{
    return GetBalance(Params.Account,Params.Currency,Params.ID);
}


"public"
function TotalAmount(Params)
{
    return OnGetBalance(0,Params.ID);
}




function REvent(Name,Params)
{
    Event({"cmd":Name,FromNum:context.FromNum,Params:Params});
}

//Lib

function CheckSum(Sum)
{
    if(Sum===0)
        return Sum;
    if(Sum && typeof Sum==="number")
        return Sum;
        
    throw "Error, need type number: "+Sum;
}
