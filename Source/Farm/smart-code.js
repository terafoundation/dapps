﻿//Smart-contract: FarmCode


// global context:
// context.BlockNum - Block num
// context.TrNum - Tx num
// context.Account - current account {Num,Currency,Smart,Value:{SumCOIN,SumCENT}}
// context.Smart - current smart {Num,Name,Account,Owner}
// context.FromNum - sender account num
// context.ToNum - payee account num
// context.Description - text
// context.Value - {SumCOIN,SumCENT}
// context.Currency - token currency
// context.ID - token ID
// context.SmartMode - 1 if run from smartcontract
// context.Tx - tx (not present in events)


//UNIT format
//"0001":
//      Name
//      Price
//      //Equipment(AND)[{Currency,ID,Property, Count}]
//      Input   (AND) [{Currency,ID,Property, Count,res}]
//      Output  (AND) [{Currency,ID,Property, Count,ProcessTime}]
//      Storage (SUM) [{Currency,ID,Property, MaxCount}]
//      Modifier      [{Currency,ID,Property, MultTime,MultCreate,MultStorage}]




"public"
function Farming(Params)
{
    var BlockNum=context.BlockNum;
    var Account=context.FromNum;
    if(!Account || typeof Account!=="number")
        throw "Error Account number";
        
    var Items0=ReadItem(0);
    
    
    var ID=String(Params.ID);
    var Items=ReadItem(Account);
    Items.Map=MapFromArr(Items.Arr);

    var Info=ReadInfo();
    var RowGener=Info.Map[ID];
    if(!RowGener || !RowGener.Output)
        throw "Error token ID="+ID;
        
    var Row=GetBalanceRow(Items,ID);
    var Amount=Row.Amount;
    if(!Amount)
        throw "Error generate amount: "+Amount;
    
    
    var TimeDelta=BlockNum-Row.BlockTime;

    
    for(var i=0;RowGener.Input && i<RowGener.Input.length;i++)
    {
        //есть список необходимых ресурсов
        var Resource=RowGener.Input[i];
        var Need=Resource.Count;
        if(Resource.res)
            Need=Need*Amount;
        
        var RowRest=GetBalanceRow(Items,Resource.ID);
        

        var BlockTime=RowRest.BlockTime;
        if(Need>RowRest.Amount)
            throw "Error resource amount: "+RowRest.Amount+"/"+Need;
        
        //тратим ресурсы   
        if(Resource.res) 
        {
            var CoinSub=COIN_FROM_FLOAT(Need);
            if(!SUB(RowRest,CoinSub))
                throw "Error RowRest SUB operation";
                
            
            var Row0=FindRow(Items0.Arr,Resource.ID);
            SUB(Row0,CoinSub);
            
        }


        //для определения сколько прошло времени - берем минимальное из всех
        if(Resource.res)
            TimeDelta=Math.min(TimeDelta,BlockNum-RowRest.BlockTime);
    }

    for(var p=0;p<RowGener.Output.length;p++)
    {
        //есть правило генерации продукта
        var Product=RowGener.Output[p];

            
        var K=(TimeDelta)/Product.ProcessTime;
        if(K<1)
            throw "Error process time for: "+Product.ID;
            
        var RowProduct=GetBalanceRow(Items,Product.ID);
        var CountCreate=Amount*Product.Count;
        
        var CoinAdd=COIN_FROM_FLOAT(CountCreate);
        ADD(RowProduct,CoinAdd);
        AddBlockTime(RowProduct,CoinAdd);
        
        var Row0=FindRow(Items0.Arr,Product.ID);
        ADD(Row0,CoinAdd);
    }
        

    Row.BlockTime=BlockNum;
    WriteItem(Items);
    WriteItem(Items0);
    
    REvent("Farming",Params);
    
}

"public"
function MarketSwap(Params)
{
    var Account=context.FromNum;
    if(!Account || typeof Account!=="number")
        throw "Error Account number";
        
    //var Items0=ReadItem(0);
    
    var Amount=CheckSum(Params.Amount)>>>0;
    var ID=String(Params.ID);
    var Items=ReadItem(Account);
    Items.Map=MapFromArr(Items.Arr);

    var Info=ReadInfo();
    var RowProd=Info.Map[ID];
    if(!RowProd)
        throw "Error token ID="+ID;

        
    var WriteMode=0;
    var Price=RowProd.SwapSale;
    if(Price)
    {
        if(RowProd.SwapCount)
            Price=RowProd.SwapStar/RowProd.SwapCount;
        else
        {
            RowProd.SwapStar=0;
            RowProd.SwapCount=0;
        }

        WriteMode=1;
    }
    
    if(Params.Mode==="Sale")
    {
        //пользователь продает даппу
        
        var Row=GetBalanceRow(Items,ID);
        if(Amount>Row.Amount)
            throw "Error amount: "+Amount+"/"+Row.Amount;
        
        var SumDelta=0;
        if(!Price)
        {
            //auto mode
            Price=RowProd.DappBuy;
        }
        else
        {
            if(RowProd.DappBuy && RowProd.DappBuy<Price)
                Price=RowProd.DappBuy;
            else
            {
                if(Amount>RowProd.SwapCount)
                    throw "Error max amount: "+Amount+"/"+RowProd.SwapCount;
                    
                
                var Price2=Price/Info.SwapPriceK;
                SumDelta=2*(Price-Price2)*Amount;
                Price=Price2;
            }
                
        }
        
        if(!Price)
            throw "Error Price="+Price;
        
        
        var Sum=Price*Amount;
        if(WriteMode)
        {
            RowProd.SwapStar-=Sum+SumDelta;
            RowProd.SwapCount-=Amount;
            
            //контроль - цена не может быть ниже SwapSale
            if(RowProd.SwapSale && RowProd.SwapCount>0 && RowProd.SwapStar/RowProd.SwapCount<RowProd.SwapSale)
            {
                //console.log(RowProd.SwapStar,RowProd.SwapCount,RowProd.SwapSale);
                RowProd.SwapStar=RowProd.SwapCount*RowProd.SwapSale;
                //console.log("New:",RowProd.SwapStar);
            }
            
            if(RowProd.SwapCount<=0 || RowProd.SwapStar<=0)
            {
                delete RowProd.SwapStar;
                delete RowProd.SwapCount;
            }

            
            WriteInfo(Info);
        }
        
        Burn(Account,ID,Amount);
        Burn(0,ID,Amount);
        

        Mint(Account,"",Sum); 
        Mint(0,"",Sum);
    }
    else
    if(Params.Mode==="Buy")
    {
        //пользователь покупает у даппа
        
        if(!Price)
        {
            //auto mode
            Price=RowProd.DappSale;
        }
        else
        {
            Price=Price*Info.SwapPriceK;
        }
        
        if(!Price)
            throw "Error Price="+Price;
        

        var Sum=Price*Amount;
        var Row=GetBalanceRow(Items,"");
        if(Sum>Row.Amount)
            throw "Error STAR amount: "+Row.Amount+"/"+Sum;
            
        if(WriteMode)
        {
            RowProd.SwapStar+=Sum;
            RowProd.SwapCount+=Amount;
            WriteInfo(Info);
        }
        
        Burn(Account,"",Sum); 
        Burn(0,"",Sum);
        
        Mint(Account,ID,Amount);
        Mint(0,ID,Amount);
    }
    
}

"public"
function Event21()
{
    var Account=context.FromNum;
    if(!Account || typeof Account!=="number")
        throw "Error Account number";
    
    var ID="77193083000";
    
    var Items=ReadItem(Account);
    if(Items.Arr.length)
        throw "Item was sent";
    
    Mint(Account,ID,1);
    Mint(0,ID,1);
    
    // ID="76620409000";
    // Mint(Account,ID,1);
    // Mint(0,ID,1);
 
    // ID="76620740000";
    // Mint(Account,ID,10);
    // Mint(0,ID,1);
   
}


//--------------------------------------------------------------------------- PostCard
 "public"
function CreatePostCard(Params)
{   
    var Account=context.FromNum;
    if(!Account || typeof Account!=="number")
        throw "Error Account number";

    var Item=ReadItem(Account);
    var ID=Params.ID;
    var Row=FindRow(Item.Arr,Params.ID);

    Burn(Account,ID,1);
    Burn(0,ID,1);
    
    if(Row.Type!=="PostCard" || Row.Status)
        throw "Error type post card = "+Row.Type+" Status:"+Row.Status;
        

    Params.Amount=1;
    var ID2=""+(context.BlockNum*1000+context.TrNum);//self
    
    Mint(Account,ID2,1,"PostCard",1);
    
    REvent("CreatePostCard",Params);
}




function GetBalanceRow(Items,ID)
{
    var Row=Items.Map[ID];
    if(!Row)
    {
        Row={ID:ID,SumCOIN:0,SumCENT:0, BlockTime:context.BlockNum};
        Items.Map[ID]=Row;
        Items.Arr.push(Row);
    }
        
    Row.Amount=FLOAT_FROM_COIN(Row);
    return Row;
}


function MapFromArr(Arr)
{
    var Map={};
    for(var i=0;Arr && i<Arr.length;i++)
    {
        var Item=Arr[i];
        Map[Item.ID]=Item;
    }
    return Map;
}








function AddBlockTime(Row,NewValue)
{
    // var WasTime=Row.BlockTime;
    // console.log(Row.ID,"Row.BlockTime:",WasTime,"->",Row.BlockTime);
    var Sum1=FLOAT_FROM_COIN(Row);
    if(!Row.BlockTime || !Sum1)
        Row.BlockTime=context.BlockNum;
    else
    {
        var Sum2=FLOAT_FROM_COIN(NewValue);
        Row.BlockTime = (Sum1*Row.BlockTime + Sum2*context.BlockNum)/(Sum1+Sum2);
    }
}



//-------------------------------------------------------------------------------------------------------------INFO
"public"
function ReadInfo()
{
    var Item=ReadValue("INFO");
    if(!Item)
        Item={Map:{}};
    return Item;
}

function WriteInfo(Item)
{
    WriteValue("INFO",Item);
}

"public"
function SetInfo(Params)
{
    CheckOwnerPermission();
    WriteInfo(Params);
}




//-------------------------------------------------------------------------------------------------------------Token Lib
function Format()
{
    return {Arr:[{ID:"str",SumCOIN:"uint",SumCENT:"uint32", BlockTime:"double",    TimeValue:"double", Type:"str",Status:"byte", Reserve:"str19"}]};
}


"public"
function InPlaceShow(Mode)
{
    // if(Mode==="Info")
    // {
    //     return "test";
    // }
    return 1;
}


//"public"
function OnTransfer(Params)
{
    //Params: From,To,Amount,ID
    if(!FLOAT_FROM_COIN(Params.Amount))
        return;

    if(Params.ID && (Params.Amount.SumCENT || typeof Params.Amount.SumCENT!=="number"))
        throw "Error Amount for NFT. Fractional values are prohibited.";

    if(Params.To<8)//burn
    {
        Burn(Params.From,Params.ID,Params.Amount);
        Burn(0,Params.ID,Params.Amount);
        return;
    }


    var Item1=ReadItem(Params.From);
    var Row1=FindRow(Item1.Arr,Params.ID);

    if(!SUB(Row1,Params.Amount))
        throw "There are not enough funds on the account: "+Params.From;
    if(ISZERO(Row1))
        DeleteRow(Item1.Arr,Params.ID);
    WriteItem(Item1);


    var Item2=ReadItem(Params.To);
    var Row2=FindRow(Item2.Arr,Params.ID);
    ADD(Row2,Params.Amount);
    Row2.Type=Row1.Type;
    Row2.Status=Row1.Status;

    AddBlockTime(Row2,Params.Amount);
    
    WriteItem(Item2);

    RegInWallet(Params.To);
}

"public"
function OnGetBalance(Account,ID)
{
    var Item=ReadItem(Account);
    if(!Item)
        Item={Arr:[]};
    if(ID!==undefined)
        return FindRow(Item.Arr,ID);

    var Arr=Item.Arr;
    for(var i=0;i<Arr.length;i++)
        if(Arr[i].ID)
            Arr[i].URI="/nft/"+Arr[i].ID;

    return Arr;
}


function ReadItem(Account)
{
    var Key="ACC:"+Account;
    var Item=ReadValue(Key,Format());
    if(!Item)
        Item={Arr:[]};
    Item.Key=Key;
    return Item;
}

function WriteItem(Item)
{
    WriteValue(Item.Key,Item,Format());
}

function FindRow(Arr,ID)
{
    if(!ID)
        ID="";
    for(var i=0;i<Arr.length;i++)
    if(Arr[i].ID==ID)
    {
        return Arr[i];
    }

    var Row={ID:ID,SumCOIN:0,SumCENT:0,BlockTime:0};
    Arr.push(Row);
    return Row;
}

function DeleteRow(Arr,ID)
{
    if(!ID)
        ID="";
    for(var i=0;i<Arr.length;i++)
    if(Arr[i].ID==ID)
    {
        Arr.splice(i,1);
        break;
    }
}


function Mint(Account,ID,Amount,Type,Status)
{
    if(typeof Amount==="number")
        Amount=COIN_FROM_FLOAT(Amount);

    var Item=ReadItem(Account);
    var Row=FindRow(Item.Arr,ID);
    if(Type)
    {
        Row.Type=Type;
        Row.Status=Status;
    }
    ADD(Row,Amount);
    
    
    
    AddBlockTime(Row,Amount);
    WriteItem(Item);

    if(Account)
        RegInWallet(Account);
        
    
}

function Burn(Account,ID,Amount)
{
    if(typeof Amount==="number")
        Amount=COIN_FROM_FLOAT(Amount);

    var Item=ReadItem(Account);
    var Row=FindRow(Item.Arr,ID);
    if(!SUB(Row,Amount))
    {
        if(!Account)
            return;
            
        throw "Error burn Amount in account: "+Account+" Lack of funds: "+(-FLOAT_FROM_COIN(Row));
    }
    
    if(ISZERO(Row))
        DeleteRow(Item.Arr,ID);
    WriteItem(Item);
}

"public"
function MintNFT(Params)
{
    CheckOwnerPermission();

    Params.Amount=1;
    Params.ID=""+(context.BlockNum*1000+context.TrNum);//self
    DoMint(Params);
}




//Owner mode

"public"
function DoMint(Params)
{
    CheckOwnerPermission();

    Mint(Params.Account,Params.ID,Params.Amount,Params.Type,Params.Status);
    Mint(0,Params.ID,Params.Amount,Params.Type,Params.Status);
}

"public"
function DoBurn(Params)
{
    CheckOwnerPermission();
    Burn(Params.Account,Params.ID,Params.Amount);
    Burn(0,Params.ID,Params.Amount);
}

function CheckOwnerPermission()
{
    if(context.FromNum!==context.Smart.Owner)
        throw "Access is only allowed from Owner account";
}

// function CheckSenderPermission()
// {
//     if(context.FromNum!==context.Account.Num)
//         throw "Access is only allowed from Sender account: "+context.FromNum+"/"+context.Account.Num;
// }


//static mode


"public"
function DoGetBalance(Params)
{
    return GetBalance(Params.Account,Params.Currency,Params.ID);
}


"public"
function TotalAmount(Params)
{
    return OnGetBalance(0,Params.ID);
}




function REvent(Name,Params)
{
    Event({"cmd":Name,FromNum:context.FromNum,Params:Params});
}

//Lib

function CheckSum(Sum)
{
    if(Sum===0)
        return Sum;
    if(Sum && typeof Sum==="number")
        return Sum;
        
    throw "Error, need type number: "+Sum;
}
