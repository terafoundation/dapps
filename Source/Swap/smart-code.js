﻿
// global context:
// context.BlockNum - Block num
// context.TrNum - Tx num
// context.Account - current account {Num,Currency,Smart,Value:{SumCOIN,SumCENT}}
// context.Smart - current smart {Num,Name,Account,Owner}
// context.FromNum - sender account num
// context.ToNum - payee account num
// context.Description - text
// context.Value - {SumCOIN,SumCENT}
// context.Currency - token currency
// context.ID - token ID
// context.SmartMode - 1 if run from smartcontract
// context.Tx - tx (not present in events)





function GetNewInfo(Rate)
{
    var Ret={};
    for(var key in SInfoFormat())
        Ret[key]=0;
    Ret.Rate=Rate;
    Ret.KProfit=0.25;
    Ret.MaxKFee=0.003;
    Ret.TimeRate={TRate:0,TStamp:0,Arr:[]};
    Ret.HistoryArr=[];
    Ret.StartTime=context.BlockNum;
    return Ret;
}


function SInfoFormat()
{
    return  { 
             Rate:"uint32",
             Pool1:"double",
             Pool2:"double",
             Total1:"double",
             Total2:"double",
             KFee:"double",
             Fee:"double",
             Fee1:"double",
             Fee2:"double",
             KProfit:"double",
             Profit:"double",
             Profit1:"double",
             Profit2:"double",
             Base1:"uint32",
             Base2:"uint32",
             Reserv1:"str50",
             TimeRate:{TRate:"uint",TStamp:"uint32", Arr:[{TRate:"uint",TStamp:"uint32"}]},
             Orders:"uint32",
             MaxKFee:"double",
             HistoryArr:[{Time:"uint32",Value1:"double",Value2:"double",Value:"double"}],
             StartTime:"uint",
             Reserv2:"str50",
            };
}

function RFormat(Type)
{
    var F={ 
            Arr:[{
                Pool:"double",
                KFee:"double",
                Swaps:"double",
            }],
            Reserv:"str10"
            };
    if(Type==="BIG")
    {
        F.WCount="uint";
        F.Arr[0].Orders="uint32";
    }
    return F;
}


function OFormat()
{
    return  { 
            Arr:[{
                ID:"uint",
                As1:"uint32",
                As2:"uint32",
                Acc1:"uint32",
                Acc2:"uint32",
                From:"uint32",
                To:"uint32",
                Amount1:"double",
                Amount2:"double",
                Stake:"double",
                Swaps0:"double",
                KFee:"double",
                State:"byte",
                Reserve:"arr20",
            }],
            Reserv:"str10"
            };
}

function UserKey(AccNum)
{
    if(!AccNum)
        AccNum=context.FromNum;
    var Obj=ReadAccount(AccNum);
    var Str=Obj.PubKeyStr.substr(0,40);
    if(Str==="0000000000000000000000000000000000000000")
        throw "Error account: "+AccNum;
    return Str;
}











"public"
function AddPool(Params)
{
    //Params: {ID, As1,As2,Acc1,Acc2, Stake, From,To, CheckRate,KFee}


    CheckBase();
    CheckTx(Params);
    //CheckNum(Params.ID,"ID");

    var Names=GetNames(Params);

    //читаем ордер
    var List=ReadOList();
    var Order=FindOrder(List,Params.ID);
    var OrderNew={
        ID:Params.ID,
        As1:Names.As1,
        As2:Names.As2,
        Acc1:Names.Acc1,
        Acc2:Names.Acc2,
        State:0,
    };
    
    var bNew;
    if(Order)
    {
        //шапка не должна меняться (так как она задает смысл между Amount1 и Amount2)
        for(var key in OrderNew)
            if(Order[key]!==OrderNew[key])
                throw "Different value in order ID:"+Params.ID+" key: "+key;
    }
    else
    {
        bNew=1;
        Order=OrderNew;
    }
    
    
    var Amount=FLOAT_FROM_COIN(context.Value);
    var Currency=context.Currency;
    if(context.ID)
        throw "NFT not supported. Receive ID="+context.ID;
    
    if(Currency===Order.As1)
        Order.Amount1=Amount;
    else
    if(Currency===Order.As2)
        Order.Amount2=Amount;
    else
        throw "Error currency="+Currency+" in order: "+Params.ID;
    
    var CountP=(Params.From<=Params.CheckRate && Params.CheckRate<=Params.To)?2:1;
    var CountA=(Order.Amount1 && Order.Amount2)?2:1;
    
    if(CountP===CountA)
    {
        //число параметров и полученных депозитов сумм совпало - заводим ликвидности в пулы
        AddNewPools(Params,Order);
        
    }


    //запись ордера
    if(bNew)
    {
        List.Arr.push(Order);
    }
    
    WriteOList(List);
    
    REvent("AddPool",Order);
    
}


function AddNewPools(Params,Order)
{
    //Params: {As1,As2,Acc1,Acc2, Stake, From,To, CheckRate}
    


    var Stake=Around(Params.Stake);
    var ResObj={Amount:0,Swaps:0};
    ResObj.From=Params.From;
    ResObj.To=Params.To;
    
    if(Order)
    {
        CheckNum(ResObj.From,"From");
        CheckNum(ResObj.To,"To");
        CheckNum(Params.KFee,"KFee",1);
        CheckNum(Stake,"Stake");
    }
    

    
    var CacheMap={};
    var Names=GetNames(Params);
    if(Names.Invert)
    {
        ResObj.From=50000-Params.To;
        Params.CheckRate=50000-Params.CheckRate;
        ResObj.To=50000-Params.From;
    }
    
    
    
    
    
    var SInfo=ReadSwap(Names);
    CheckPair(SInfo,Names);

    
    
    var Rate=SInfo.Rate;

    DistribLiq(CacheMap,Names,Stake,Params.KFee,ResObj);
    FillAmounts(ResObj,Stake,Rate,1);

    if(!Order)
        return {Amount1:Names.Invert?ResObj.Amount2:ResObj.Amount1,Amount2:Names.Invert?ResObj.Amount1:ResObj.Amount2};

    
    if(Params.CheckRate && SInfo.Rate!=Params.CheckRate)
        throw "Error CheckRate = "+Params.CheckRate+"/"+SInfo.Rate;


    //Max fee = 0.5%
    if(SInfo.MaxKFee && Params.KFee>SInfo.MaxKFee)
        throw "Error KFee="+Params.KFee+" Max="+SInfo.MaxKFee;
        
    if(ResObj.From>ResObj.To)
        throw "Error range";
    if(ResObj.Amount1<=0 && ResObj.Amount2<=0)
        throw "Need range amounts";

    if(!Order.Amount1)
        Order.Amount1=0;
    if(!Order.Amount2)
        Order.Amount2=0;
    
    var Delta1=Order.Amount1-ResObj.Amount1;
    var Delta2=Order.Amount2-ResObj.Amount2;
    
    //console.log("Delta1:",Delta1,"Delta2:",Delta2,"Order:",Order,"ResObj:",ResObj);
    
    if(Delta1<0)
        throw "Error Amount1 in order: "+Order.ID+" Delta="+Delta1+"  ("+ResObj.Amount1+"/"+Order.Amount1+")";
    if(Delta2<0)
        throw "Error Amount2 in order: "+Order.ID+" Delta="+Delta2+"  ("+ResObj.Amount2+"/"+Order.Amount2+")";
    
     
     //edit order!!
    Order.From=ResObj.From;
    Order.To=ResObj.To;
    Order.Stake=Stake;
    Order.KFee=Params.KFee,
    Order.Swaps0=ResObj.Swaps;
    Order.State=1;
       
        

    //cur pool swap
    if(Rate>=ResObj.From && Rate<=ResObj.To)
    {
        
        //корректировка KFee
        SetKFee(SInfo,Stake,Order.KFee);
        
        //заполнение текущей позиции пулов
        SInfo.Pool1+=Stake/2;
        SInfo.Pool2+=Stake/2;

    }
    
    SInfo.Total1+=Order.Amount1;
    SInfo.Total2+=Order.Amount2;
    SInfo.Orders++;
    
    
    
    
    WriteMRanges(CacheMap,1);
    WriteSwap(SInfo,Names);
    
}



"public"
function RemLiq(Params)
{
    //Params: {Num,ID,OnlyFee}

    CheckBase();
    CheckTx(Params);

    var List=ReadOList();
    var Order=List.Arr[Params.Num];
    if(!Order || Order.ID!==Params.ID)
    {
        
        var Order=FindOrder(List,Params.ID);
        if(!Order)
            throw "Error order with ID="+Params.ID;
            
        Params.Num=Order.PosIndex;
    }
    
    var Names=GetNames(Order);
    var SInfo=ReadSwap(Names);
    
    if(!Order.State)
    {
        //not runing order
        
        List.Arr.splice(Params.Num,1);
        WriteOList(List);
        
        if(Order.Amount1>0)
            MoveT(SInfo.Base1, 0,Order.Acc1,Names.As1,Order.Amount1,"Withdraw1");
            
        if(Order.Amount2>0)
            MoveT(SInfo.Base2, 0,Order.Acc2,Names.As2,Order.Amount2,"Withdraw2");
        
        
        REvent("RemLiq",Order);
        return;
    }
    

    var Stake=Order.Stake;
    //console.log("ORDER:",Order);

    var CacheMap={};

    var Swaps0=Order.Swaps0;
    var Rate=SInfo.Rate;
    
    var ResObj={Amount:0,Amount1:0,Amount2:0,Swaps:0,From:Order.From,To:Order.To};
    if(!Params.OnlyFee)
    {
        SInfo.Orders--;
        Order.State=2;
        DistribLiq(CacheMap,Names,-Stake,Order.KFee,ResObj);
        ResObj.Amount=-ResObj.Amount;
        FillAmounts(ResObj,Stake,Rate,0);
        
        
        //cur pool swap
        if(Rate>=ResObj.From && Rate<=ResObj.To)
        {
            var Pool=SInfo.Pool1+SInfo.Pool2;
            if(Pool>0)
            {
                //корректировка KFee
                SetKFee(SInfo,-Stake,Order.KFee);
                
                var FRate=GetFRate(Rate);
                

                //коректировка Amount пропорционально текущим пулам
                var KK=Stake/Pool;
                ResObj.Amount1+=SInfo.Pool1*KK;
                ResObj.Amount2+=FRate*SInfo.Pool2*KK;
                

                //корректировка пулов
                SInfo.Pool1=(1-KK)*SInfo.Pool1;
                SInfo.Pool2=(1-KK)*SInfo.Pool2;
                
                //console.log("=POOLS=",SInfo.Pool1,SInfo.Pool2);
                
            }
        }
        
        
        //remove order
        List.Arr.splice(Params.Num,1);
    }
    else
    {
        DistribLiq(CacheMap,Names,0,0,ResObj);
        
        //edit order
        Order.Swaps0=ResObj.Swaps;
    }
   
    //console.log(ResObj);
    
    if(SInfo.Fee)
    {
        var Delta=ResObj.Swaps-Swaps0;
        var Fee=Delta*Stake*Order.KFee;
        var KK=Fee/SInfo.Fee;
        var Fee1=KK*SInfo.Fee1;
        var Fee2=KK*SInfo.Fee2;
        
        var P=SInfo.KProfit;

        SInfo.Fee-=Fee;
        SInfo.Fee1-=Fee1;
        SInfo.Fee2-=Fee2;

        SInfo.Profit+=Fee*P;
        SInfo.Profit1+=Fee1*P;
        SInfo.Profit2+=Fee2*P;

        
        //console.log("Swaps:",Swaps,"Fee1=",Fee1,"Fee2=",Fee2);
        
        ResObj.Amount1+=Fee1*(1-P);
        ResObj.Amount2+=Fee2*(1-P);
    }


    SInfo.Total1-=ResObj.Amount1;
    SInfo.Total2-=ResObj.Amount2;

    WriteOList(List);
    WriteMRanges(CacheMap,1);
    WriteSwap(SInfo,Names);

    
    
    if(ResObj.Amount1>0)
        MoveT(SInfo.Base1, 0,Order.Acc1,Names.As1,ResObj.Amount1,"Withdraw1");
        
    if(ResObj.Amount2>0)
        MoveT(SInfo.Base2, 0,Order.Acc2,Names.As2,ResObj.Amount2,"Withdraw2");
    
    
    //console.log("SInfo:",SInfo);

    // if(SInfo.Orders==0 && (SInfo.Total1>1e-6 || SInfo.Total2>1e-6))
    //     throw "Error REST TOTAL: "+SInfo.Total1+", "+SInfo.Total2;
    

    REvent("RemLiq",Order);
}






function GetBase(Info,Asset)
{
    if(!Asset || Asset==="TERA" || Asset=="0")
        return 0;
        
    var Base=Info.Assets[Asset];
    if(Base!==undefined)//уже была запись
        return Base;
        
        
    Asset=+Asset;
    
    //смотрим тип смартконтракта, 2 - новая версия всегда soft token
    var Smart=ReadSmart(Asset);
    if(Smart && Smart.Version>=2)
        return 0;

    throw "Not found Base account for asset: "+Asset;
}


"public"
function CreateSwap(Params)
{
    //Params: {As1,As2, InitRate}

    //Event("Got from "+ context.FromNum+" = "+JSON.stringify(context.Value)+" Currency:"+context.Currency);
    
    
    CheckBase();
    CheckTx(Params);

    CheckNum(Params.InitRate,"InitRate");
    
    var Names=GetNames(Params);
    var SInfo=ReadSwap(Names);
    if(!SInfo)
    {
        var Common=GetCommon();
        if(Common.PriceCreate)
        {
            var GotValue=FLOAT_FROM_COIN(context.Value);
            if(context.ID)
                throw "NFT not supported. Receive ID="+context.ID;
            
            if(context.Currency!==Common.CurrencyCreate || GotValue<Common.PriceCreate)
                throw "Price for create swap: "+Common.PriceCreate+" of currency: "+Common.CurrencyCreate+" (got: "+GotValue+" currency: "+context.Currency+")";
        }
        
        
        if(Names.Invert)
            Params.InitRate=50000-Params.InitRate;
        
        SInfo=GetNewInfo(Params.InitRate);
        

        
        SInfo.Base1=GetBase(Common,Names.As1);
        SInfo.Base2=GetBase(Common,Names.As2);
        
        Common.Assets[Names.As1]=SInfo.Base1;
        Common.Assets[Names.As2]=SInfo.Base2;
        WriteCommon(Common);
        
        
        WriteSwap(SInfo,Names);
        
        //Move(context.FromNum,context.Smart.Account, 100, "Creating swap pair");

        REvent("CreateSwap",Params);
    }
}


"public"
function Swap2(Params)
{
    //Params: {As1,As2,As3,ToAccount, Rate1,Rate2,Slippage1,Slippage2}
    
    //TODO - проверить атаку повторного  входа!!!      Refund           <-----------------------------------------------
    
    var Params1={ToAsset:Params.As2, ToAccount:0, Rate:Params.Rate1,Slippage:Params.Slippage1};
    var Amount=Swap(Params1,[],0,1);

    var Params2={ToAsset:Params.As3, ToAccount:Params.ToAccount, Rate:Params.Rate1, Slippage:Params.Slippage2, FromAmount:Amount,FromAsset:Params.As2};
    Swap(Params2,[],0,2);
    
    REvent("Swap2",Params);
}



"public"
function Swap(Params ,ParamsArr,bInfo,FromSwap2)
{
    //Params: {ToAsset,ToAccount,Rate,Slippage}

    var Slippage=+Params.Slippage;
    if(!Slippage)
        Slippage=0;
    var CheckRate=+Params.Rate
    if(!CheckRate)
        CheckRate=0;
    

    var PayAmount=FLOAT_FROM_COIN(context.Value);
    var PayCurrency=context.Currency;
    if(context.ID)
        throw "NFT not supported. Receive ID="+context.ID;
    
    if(bInfo || FromSwap2===2)
    {
        PayAmount=Params.FromAmount;
        PayCurrency=Params.FromAsset;
    }
    

    var Names=GetNames({As1:PayCurrency,As2:Params.ToAsset});
    var Mode=Names.Invert?2:1;


    var SInfo=ReadSwap(Names);
    if(!SInfo)
    {
        if(bInfo)
            return;
            
        CheckPair(SInfo,Names);
    }
    
    //const
    var KSteps=100;
    var K=1000, W=25000;
    
    var CacheMap={};


    var Amount1=0,Amount2=0,Fee=0,Fee1=0,Fee2=0;
    var RestA=PayAmount;
    var Tiks=0;
    var Big=ReadRange(CacheMap,Names,"BIG");
    var BIndexWas=Math.floor(SInfo.Rate/KSteps);
    var Detail=ReadRange(CacheMap,Names,BIndexWas);
    
    
    if(CheckRate && Math.abs(CheckRate-SInfo.Rate)>Slippage)
        throw "Reached max value of Slippage = "+Slippage;

    
    var FRate;
    while(SInfo.Rate>=0 && RestA>0 && SInfo.Rate<=50000)
    {

        if(Tiks>=60)
            break;
        Tiks++;
        
        
        FRate=GetFRate(SInfo.Rate);
        
        FRate=K*Math.exp((SInfo.Rate+0.5-W)/K)-K*Math.exp((SInfo.Rate-0.5-W)/K);
        
        //FRate2
        var CanSum,DeltaI,RestPool;
        
        //Mode=1  1->2 (идем влево по массиву)
        //Mode=2  2->1 (идем вправо по массиву)
        if(Mode==1)
        {
            DeltaI=-1;
            CanSum=Math.min(RestA,SInfo.Pool2);
            SInfo.Pool1+=CanSum;
            SInfo.Pool2-=CanSum;
            RestA-=CanSum;
            //FRate2=FRate/MM;
            RestPool=SInfo.Pool2;
            Fee2+=CanSum*SInfo.KFee*FRate;
        }
        else
        {
            DeltaI=+1;
            CanSum=Math.min(RestA/FRate,SInfo.Pool1);
            SInfo.Pool2+=CanSum;
            SInfo.Pool1-=CanSum;
            RestA-=CanSum*FRate;
            //FRate2=FRate*MM;
            RestPool=SInfo.Pool1;
            Fee1+=CanSum*SInfo.KFee;
        }
        

        //add Swaps count
        var AllPool=SInfo.Pool1+SInfo.Pool2;
        SetSwaps(Big,BIndexWas,AllPool,CanSum);
        SetSwaps(Detail,SInfo.Rate%KSteps,AllPool,CanSum);
        
        Amount1+=CanSum;
        Amount2+=CanSum*FRate;
        Fee+=CanSum*SInfo.KFee;
       
        
        if(RestPool>=1e-9)
        {
            break;
        }
        
        //next range
        
        SInfo.Rate+=DeltaI;
        //FRate=FRate2;
        
        //load pools
        var BIndex=Math.floor(SInfo.Rate/KSteps);
        if(BIndex!=BIndexWas)
        {
            BIndexWas=BIndex;
            Detail=ReadRange(CacheMap,Names,BIndex);
        }
        
        
        var Set={Pool:0,KFee:0};
        SetPool(Detail,SInfo.Rate%KSteps,1,Set);
        SetPool(Big,BIndex,KSteps,Set);
        
        SInfo.KFee=Set.KFee;
        if(Mode==1)
        {
            SInfo.Pool1=0;
            SInfo.Pool2=Set.Pool;
        }
        else
        {
            SInfo.Pool1=Set.Pool;
            SInfo.Pool2=0;
        }
    }
    
    
    
    
    SInfo.Fee+=Fee;
    SInfo.Fee1+=Fee1;
    SInfo.Fee2+=Fee2;

    Amount1=Around(Amount1-Fee1);
    Amount2=Around(Amount2-Fee2);

    var List=[{Base:SInfo.Base1,Amount:Amount1,M:1},{Base:SInfo.Base2,Amount:Amount2,M:-1}];
    var ItemG=List[Mode-1];
    var ItemS=List[Mode%2];
    
    if(bInfo)
        return {Amount1:ItemG.Amount,Amount2:ItemS.Amount,Rate:Mode==1?FRate:1/FRate};
    
    CheckTx(Params);
    CheckBase();
    
    if(SInfo.StartTime>context.BlockNum)
        throw "Cannt SWAP, StartTime:"+StartTime;
    

        
    
            
    
    SInfo.Total1+=ItemG.M*Amount1;
    SInfo.Total2+=ItemS.M*Amount2;
    AddStat(SInfo,Amount1,Amount2,Mode==1?Amount1:-Amount1);
    WriteMRanges(CacheMap);
    WriteSwap(SInfo,Names);


    var Delta=PayAmount-ItemG.Amount;
    if(Delta>1e-9  && FromSwap2!==2)
    {
        MoveT(ItemG.Base, 0,context.FromNum,PayCurrency,Delta,"Refund");
    }
    
    if(ItemS.Amount>0 && FromSwap2!==1)
    {
        MoveT(ItemS.Base, 0, Params.ToAccount,Params.ToAsset,ItemS.Amount,"Swap2");
    }
        
    if(FromSwap2)
    {
        return ItemS.Amount;
    }
        

    REvent("Swap",Params);
}


function SetSwaps(Range,Index,AllPool,SwapSum)
{
    var Item=Range.Arr[Index];
    
    if(!Item || !Item.Pool)
        return;
        
    //число оборотов единицы пула
    var KSwap=SwapSum/AllPool;
    if(!KSwap)
        return;
        
    Range.Write=1;
    if(!Item.Swaps)
        Item.Swaps=0;
    Item.Swaps+=KSwap;
}






//-------------------------------------- common part
"public"
function AssetInfo(Params)
{
    //Params: {As1,As2}

    var Ret={Rate:0};
    if(Params.Info)
        Ret.Info=GetCommon();
        
    var Names=GetNames(Params);
    var SInfo=ReadSwap(Names);
    if(SInfo)
    {
        var Rate=SInfo.Rate;
        if(Names.Invert)
            Rate=50000-Rate;
        Ret.Rate=GetFRate(Rate);
        Ret.IRate=Rate;
        
        if(Params.SInfo)
            Ret.SInfo=SInfo;
        if(Params.Swap)
            Ret.Swap=Swap(Params,[],1);
            
        if(Params.Ranges)
        {
            var Arr;
            var Big=ReadRange({},Names,"BIG");
            if(Big.WCount>=Params.Ranges || Big.ID!==Params.RangeID)
            {
                Arr=CopyNZ(Big.Arr);
                for(var i=0;i<Arr.length;i++)
                    if(Arr[i] && Arr[i].Orders)
                        Arr[i].SubArr=CopyNZ(ReadRange({},Names,i).Arr);
            }
            Ret.Ranges={ID:Big.ID,WCount:Big.WCount,Arr:Arr};
        }
        
    }
    if(Params.Orders && Params.Account)
        Ret.Orders=ReadOList(Params.Account);

    
    return Ret;
}




function AddStat(SInfo,Sum1,Sum2,Sum)
{
    AddTRate(SInfo);
    SInfo.HistoryArr.unshift({Time:context.BlockNum,Value1:Sum1,Value2:Sum2,Value:Sum});
    CombiningHistory(SInfo,10);
}


function CombiningHistory(SInfo,Count)
{
    //суммирование одинаковых по времени позиций
    var Arr=SInfo.HistoryArr;
    var Time0=context.BlockNum;
    var Prev;
    for(var i=0;i<Count;i++)
    {
        var Item=Arr[i];
        if(!Item)
            break;
        Item.Num=Math.floor(6*Math.log(1+Time0-Item.Time));
        if(Prev && Prev.Num==Item.Num)
        {
            Prev.Value1+=Item.Value1;
            Prev.Value2+=Item.Value2;
            Prev.Value+=Item.Value;
            Arr.splice(i,1);
            i--;
            continue;
        }
        Prev=Item;
    }
}

"public"
function DoHistory(Params)
{
    var Names=GetNames(Params);
    var SInfo=ReadSwap(Names);
    if(SInfo)
    {
        CombiningHistory(SInfo,200);
        WriteSwap(SInfo,Names);
        Event("DoHistory")
    }
}

function AddTRate(SInfo)
{
    var Item=SInfo.TimeRate;
    var Rate=SInfo.Rate;
    var TStamp=context.BlockNum;
    if(!Item.TStamp)
        Item.TRate=Rate;
    else
        Item.TRate+=Rate*(TStamp-Item.TStamp);
    Item.TStamp=TStamp;

    

    var Arr=Item.Arr;
    var LastTime=0;
    if(Arr.length)
        LastTime=Arr[Arr.length-1].TStamp;
    if(TStamp-LastTime<3600)
        return;
        
    Arr.push(Item);
    
    if(Arr.length>80)//check - max 10 days
        Arr.shift();
        
    //console.log("=============",Arr.length);
    
}




function WriteMRanges(Map,bAll)
{
    for(var key in Map)
    {
        var Item=Map[key];
        if(bAll || Item.Write)
            WriteRange(Item);
    }
}

function SetPool(Range,Index,Mult,Set)
{
    var Item=Range.Arr[Index];
    if(Item && Item.Pool)
    {
        var Stake=Item.Pool/Mult;
        
        var WKFee1=Set.KFee*Set.Pool;
        Set.Pool+=Stake;
        Set.KFee=Set.Pool>0?(WKFee1+Item.KFee*Stake)/Set.Pool:0;
        //console.log(Set);
    }
}

function SetKFee(SInfo,Stake,KFee)
{
    var Pool=SInfo.Pool1+SInfo.Pool2;
    var Pool2=Pool+Stake;
    
    SInfo.KFee=Pool2?(Pool*SInfo.KFee+Stake*KFee)/Pool2:0;
}

function CheckPair(SInfo,Names)
{
    if(!SInfo)
        throw "Swap pair not found: "+Names.As1+"/"+Names.As2;
}






function SegmentLR(From,Rate,To)
{
    //справа от Rate область курсов Amount1
    //слева от Rate область курсов Amount2

    var M=0,CountL=0,CountR=0;
    if(From<=Rate && Rate<=To)
        M=1;

    if(From>=Rate)
        CountR=To-From+1-M;
    else
    if(To<=Rate)
        CountL=To-From+1-M;
    else
    {
        CountL=Rate-From;
        CountR=To-Rate;
    }

    return {CountL:CountL<0?0:CountL,CountR:CountR<0?0:CountR,M:M};
}


function FillAmounts(ResObj,Stake,Rate,UseM)
{
    var LR=SegmentLR(ResObj.From,Rate,ResObj.To);

    var Delta1=LR.CountR;
    var Delta2=LR.CountL;

    //Right значения Amount1
    //Left  значения Amount2
    
    
    ResObj.Amount1=Delta1*Stake;
    ResObj.Amount2=Stake*GetIntegralRate(ResObj.From,ResObj.From+Delta2-1);
    var M=UseM*LR.M/2;
    if(M)
    {
        ResObj.Amount1+=Stake/2;
        ResObj.Amount2+=Stake*GetFRate(Rate)/2;
    }
    
    //console.log("FILLS","Deltas=",Delta1,Delta2,"Rates:",ResObj.From,Rate,ResObj.To,"Amounts:",ResObj.Amount1,ResObj.Amount2)
}



function DistribLiq(Map,Names,RAmount,KFee,ResObj)
{
    
    var KSteps=100;
    var From=ResObj.From;
    var To=ResObj.To;
    ResObj.Count=1+To-From;
    if(ResObj.Count>0)
    {
        var Big=ReadRange(Map,Names,"BIG");

        var BFrom=Math.floor(From/KSteps);
        var BTo=Math.floor(To/KSteps);
        
        //detail pool left
        if(From%KSteps!==0)
        {
            SetOrders(Big,BFrom,RAmount);
            var Detail1=ReadRange(Map,Names,BFrom);
            var From=From%KSteps;
            var To1;
            if(BFrom===BTo)
                To1=To%KSteps;
            else
                To1=KSteps-1;
                
            LiqToRange(Detail1,From,To1,RAmount,KFee,ResObj);

            BFrom++;
            //console.log("BFrom",BFrom);
        }
        
        
        //detail pool right
        if(To%KSteps!==KSteps-1 && BFrom<=BTo)
        {
            SetOrders(Big,BTo,RAmount);
            var Detail2=ReadRange(Map,Names,BTo);
            var To2=To%KSteps;

            LiqToRange(Detail2,0,To2,RAmount,KFee,ResObj);

            
            
            BTo--;
            //console.log("BTo",BTo);
        }
        
        //big pool
        if(BTo>=BFrom)
        {
            LiqToRange(Big,BFrom,BTo,RAmount*KSteps,KFee,ResObj);
            Big.WCount++;
        }
        
        
        
    }
    
}

//статистика числа ордеров
function SetOrders(Big,Ind,Stake)
{
    var Item=GetRItem(Big.Arr,Ind);
    if(Stake>0)
        Item.Orders++;
    if(Stake<0)
        Item.Orders--;
        
    Big.WCount++;
}

function GetRItem(Arr,i)
{
    var Item=Arr[i];
    var Item=Arr[i];
    if(!Item || !Item.Pool)
    {
        Item={Pool:0,KFee:0,Swaps:0,Orders:Item?Item.Orders:0};
        Arr[i]=Item;
    }

    return Item;
}

function LiqToRange(Range,From,To,Stake,KFee,ResObj)
{
    var Arr=Range.Arr;
    var WKFee2=KFee*Stake;
    for(var i=From;i<=To;i++)
    {
        //var Item=GetRItem(Range.Arr,i);
        var Item=Arr[i];
        if(!Item || !Item.Pool)
        {
            Item={Pool:0,KFee:0,Swaps:0,Orders:Item?Item.Orders:0};
            Arr[i]=Item;
        }
        


        var WKFee1=Item.KFee*Item.Pool;
        Item.Pool+=Stake;
        Item.KFee=Item.Pool>0?(WKFee1+WKFee2)/Item.Pool:0;
        ResObj.Amount+=Stake;
        ResObj.Swaps+=Item.Swaps
        Item.Pool=Item.Pool<1e-9?0:Item.Pool;
    }
    //console.log("LiqToRange:"+Range.ID,From,To,Stake,KFee,Range.Arr);
}





function ReadRange(Map,Names,Start)
{
    var ID=Names.Key+":"+Start;
    
    var Item=Map[ID];
    if(Item)
        return Item;
        
    Item=ReadValue(ID,RFormat(Start));
    if(!Item || !Item.Arr)
        Item={Arr:[],WCount:0};
        
    Item.ID=ID;
    Item.Start=Start;

    Map[ID]=Item;
    //console.log("ReadRange=",Item);
    
    return Item;
}

function WriteRange(Item)
{
  WriteValue(Item.ID,Item,RFormat(Item.Start));
}





function ReadOList(Account)
{
    var Key="ORDERS:"+UserKey(Account);
    var Item=ReadValue(Key,OFormat());
    if(!Item || !Item.Arr)
    {
        Item={Arr:[]};
    }
    Item.ID=Key;
    return Item;
}

function WriteOList(Item)
{
    if(Item.Arr.length)
        WriteValue(Item.ID,Item,OFormat());
    else
        RemoveValue(Item.ID);
}

function WriteNewOrder(Order)
{
    var Item=ReadOList();
    Item.Arr.push(Order);
    WriteOList(Item);
}

function FindOrder(List,ID)
{
    for(var i=List.Arr.length-1;i>=0;i--)
    {
        var Item=List.Arr[i];
        if(Item.ID==ID)
        {
            Item.PosIndex=i;
            return Item;
        }
        
        //смотрим может нет смысла искать
        if(Item.ID<ID-100*1000)
            break;
    }
}


//-------------------------------------- Token Sends
function MoveT(Base0, From,To,Asset,Amount,Text)
{
    var Base=Base0;
    if(!Base)
        Base=context.Smart.Account;
    From=From?From:Base;
    To=To?To:Base;
    
    var Currency=Base0?0:parseInt(Asset);
    
    Event("MoveT Currency:"+Asset+" Amount:"+Amount+"    "+From+"->"+To+"  "+Text+(Currency?" Currency:"+Currency:""));
    
    Move(From,To, COIN_FROM_FLOAT2(Amount), Text, Currency);
}



function CopyNZ(Arr0)
{
    var Arr=[];
    for(var i=0;i<Arr0.length;i++)
        if(Arr0[i].Orders || Arr0[i].Pool)
            Arr[i]=Arr0[i];
    return Arr;
}

function ReadSwap(Names)
{
    return ReadValue(Names.Key,SInfoFormat());
}
function WriteSwap(SInfo,Names)
{
    WriteValue(Names.Key,SInfo,SInfoFormat());
}


"public"
function GetSwap(Params)
{
    var Names=GetNames(Params);
    return ReadSwap(Names);
}


//----------------------------------------------- Libs
function CheckTx(Params)
{
    //console.log("Delta=",Params.MaxBlock-context.BlockNum);
    if(Params.MaxBlock && Params.MaxBlock<context.BlockNum)
        throw "Error max block";
}

function CheckNum(Value,Name,bCanZero)
{
    if(typeof Value!=="number")
        throw "Error type value "+Name+" = "+Value;

    if(bCanZero && Value===0)
        return;
        
    if(!Value || Value<1e-9 || Value>1e10)
        throw "Error value "+Name+" = "+Value;
}
function CheckAsset(Asset)
{
    if(typeof Asset!=="number" || (!Asset && Asset!==0) || Asset<0 || Asset>1e9)
        throw "Error asset Asset: "+Asset;
        
    return Asset;
}




function GetFRate(Index)
{
    
    return GetIntegralRate(Index,Index);
}
function GetIntegralRate(From,To)
{
    if(To>=From)
        return GetF(To+0.5)-GetF(From-0.5);
    else
        return 0;
}

function GetF(x)
{
    return 1000*Math.exp((x-25000)/1000);
}



function Around(Sum,Negative)
{
    Sum=Math.floor(1e9*Sum)/1e9;
    if(!Negative && Sum<1e-9)
        Sum=0;
    return Sum;
}

function GetNames(Item)
{
    var As1=CheckAsset(Item.As1);
    var As2=CheckAsset(Item.As2);

    var Ret={};
    if(As1<As2)
    {
        Ret.As1=As1;
        Ret.As2=As2;
        Ret.Acc1=Item.Acc1;
        Ret.Acc2=Item.Acc2;
    }
    else
    {
        Ret.As1=As2;
        Ret.As2=As1;
        Ret.Acc1=Item.Acc2;
        Ret.Acc2=Item.Acc1;
        Ret.Invert=1;
    }
    Ret.Key="SWAP:"+Ret.As1+"-"+Ret.As2;
    return Ret;
}


//-----------------------------------------------


function WriteCommon(Item)
{
    WriteValue("INFO",Item);
}

"public"
function SetCommon(Params)
{
    CheckOwner();
    

    WriteCommon(Params);
}
"public"
function GetCommon()
{
    var Item=ReadValue("INFO")
    if(!Item)
        Item={Assets:{},PriceCreate:100,CurrencyCreate:0};
    return Item;
}







//Lib
function REvent(Name,Data)
{
    Event({"cmd":Name,FromNum:context.FromNum,Data:Data});
}


function CheckOwner()
{
    if(context.FromNum!==context.Smart.Owner)
        throw "Access is only allowed from Owner account: "+context.FromNum+"/"+context.Smart.Owner;
}
// function CheckSender()
// {
//     if(context.FromNum!==context.Account.Num)
//         throw "Access is only allowed from Sender account: "+context.FromNum+"/"+context.Account.Num;
// }

function CheckBase()
{
    if(!context.FromNum)
        throw "Not set FromNum";
        
    if(!IsZKey(context.Account.PubKeyStr) && context.Account.Num!==context.Smart.Account)
        throw "Need use base smart Account. Current="+context.Account.Num+" pubkey="+context.Account.PubKeyStr;
}


function COIN_FROM_FLOAT2(Sum)
{
    var MAX_SUM_CENT = 1e9;
    var SumCOIN = Math.floor(Sum);
    var SumCENT = Math.floor(Sum * MAX_SUM_CENT - SumCOIN * MAX_SUM_CENT);
    var Coin = {SumCOIN:SumCOIN, SumCENT:SumCENT};
    return Coin;
}



// function COIN_FROM_FLOAT3(Sum)
// {
//     var MAX_SUM_CENT = 1e9;
//     var SumCOIN=Math.floor(Sum);
//     var SumCENT = Math.floor((Sum+0.0000000001) * MAX_SUM_CENT - SumCOIN * MAX_SUM_CENT);
//     if(SumCENT>=1e9)
//     {
//         SumCENT=0;
//         SumCOIN++;
//     }
//     return {SumCOIN:SumCOIN,SumCENT:SumCENT};
// }



"public"
function DoTestCreate(Params)
{
    //Call(7,"Swap",Params,undefined,100,107,10,"");
    CheckOwner();
    
    //Params: {As1,As2, InitRate}
    MoveCall(100,107,100,"",0,"","CreateSwap",Params);

    Params.ID=context.BlockNum*1000+context.TrNum;
    Params.Acc1=0;
    Params.Acc2=0;
    Params.KFee=0.002;
    Params.Stake=100;
    Params.From=Params.InitRate;
    Params.To=Params.InitRate+10;
    Params.CheckRate=Params.InitRate;
    
    //Params: {ID, As1,As2,Acc1,Acc2, Stake, From,To, CheckRate,KFee}
    MoveCall(103,107,11,"",3,"","AddPool",Params);
    MoveCall(100,107,1050,"",0,"","AddPool",Params);
    
    Params={};
    Params.ToAsset=0;
    Params.ToAccount=106;
    
    
    //Params: {ToAsset,ToAccount,LimitRate}
    MoveCall(103,107,1,"",3,"","Swap",Params);
}


// "public"
// function DoTestSwap(Params)
// {
//        CheckOwner();
//     var Params2=
//     {
//         ToAsset:Params.As2,
//         ToAccount:Params.Acc2,
//         LimitRate:Params.LimitRate,
//         Amount:Params.Amount
//     }
    
//     //Params: {ToAsset,ToAccount,LimitRate}
//     MoveCall(Params.Acc1, context.Smart.Account, Params.Amount,"",Params.As1,"","Swap",Params2);
// }




"public"
function DelPair(Params)
{
    CheckOwner();
    var Names=GetNames(Params);
    
    RemoveValue(Names.Key);
}





"public"
function GetKey(Params)
{
    return ReadValue(Params.Key,Params.Format);
}




function IsZKey(Key)
{
    if(Key=="000000000000000000000000000000000000000000000000000000000000000000")
        return 1;
    else
        return 0;
}

"public"
function OnSetSmart()//linking a account to this smart
{
    //если это base account - то сохраняем в таблице INFO
    
    var Currency=context.Account.Currency;
    if(Currency && context.Account.Name.substr(0,10)=="SWAP BASE:" && IsZKey(context.Account.PubKeyStr))
    {
        
        var Common=GetCommon();
        if(Common.Assets[Currency])
            return Event("Was base account for currency: "+Currency);
            
        Common.Assets[Currency]=context.Account.Num;
        WriteCommon(Common);
        

        Event("Created base account for currency: "+Currency);
    }
    
}


