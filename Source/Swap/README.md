﻿## TeraSwap

https://terawallet.org/dapp/164

TeraSwap is a fully decentralized application for the exchange of tokens (assets) based on AM trading.

The main idea: liquidity providers set the price range within which automatic trading takes place, as motivation they receive a commission for the exchange.

The goal of the project is to create a large decentralized platform for the exchange of arbitrary cassettes and earning commission from exchange operations.

 

more detail:

ENG: https://docs.google.com/document/d/1673uvteysXyd9tAreugniaCTSKqaM0UqVojSHtRiKWE/edit?usp=sharing

RUS:  https://docs.google.com/document/d/158UCT-7lzPH-qqQxIfsc3JK_RrhT3vsoeIUqx4BLEfA/edit?usp=sharing
