﻿


function FormatUnit()
{
    return {
                Account:"uint32",
                BlockNum:"uint32",
                TrNum:"uint16",
                Amount:"uint32",
                Fee:"uint32",
                Egg:"uint16",
                Item:"uint16",
                Direct:"byte",
                Message:"byte",
                WinAmount:"uint32",
                
                Reserve:"str6"
           }
}

function FormatMap()
{
    return  {
                SizeX:"uint16",
                SizeY:"uint16",
                StartItems:"uint16",
                FinishItems:"uint16",
                PeriodEgg:"uint32",
                
                MinAmount:"uint32",
                MaxAmount:"uint32",
                GamePeriod:"uint32", 
                
                AwardPool:"uint32",
                AwardK:"uint16",
                AwardPeriod:"uint32",
                AwardMinCount:"uint16",
                AwardMaxCount:"uint16",
                
                MinAwardAmount:"uint32",
                MaxAwardAmount:"uint32",
                Reserve1:"str100",
                
                AReward:"byte",
                APool:"byte",
                
                RowArr:[[FormatUnit()]],
                TransferArr:[FormatUnit()],

                
                AwardCount:"uint16",
                LastAwardTime:"uint32",
                GameFee:"uint32",
                
                
                Reserve2:"str100"
            };
}


function ReadMap(ID)
{
    var Map=ReadValue("MAP"+ID,FormatMap());
    if(Map)
        Map.ID=ID;
    return Map;
}

function WriteMap(Item)
{
    WriteValue("MAP"+Item.ID,Item,FormatMap());
}




function GetDirectPos(x,y,Direct)
{
    if(Direct==1)
        y--;
    else
    if(Direct==2)
    {
        x++;
        y--;
    }
    else
    if(Direct==3)
        x++;
    else
    if(Direct==4)
    {
        x++;
        y++;
    }
    else
    if(Direct==5)
        y++;
    else
    if(Direct==6)
    {
        x--;
        y++;
    }
    else
    if(Direct==7)
        x--;
    else
    if(Direct==8)
    {
        x--;
        y--;
    }
    return {x:x,y:y}
}


function GetItemAt(Map,x,y,bSet)
{
    var BlockNum=GetCurBlockNum();
    var Row=Map.RowArr[y-1];
    if(Row)
    {
        var Item=Row[x-1];
        if(Item && Item.Item)
        {
            var Delta=BlockNum-Item.BlockNum;
            if(Item.Item==1 && Delta>Map.PeriodEgg)
                return undefined;
            if(Item.Item>=Map.StartItems && Delta>Map.GamePeriod)
            {
                if(bSet)
                {
                    //помещаем в массив трансфера
                    Map.TransferArr.push(Item);
                    SetItemAt(Map,x,y,{});
                }
                return undefined;
            }

            return Item;
        }
    }
}

function GetCurBlockNum()
{
    return context.BlockNum;
}

function SetItemAt(Map,x,y,Item)
{
    var Row=Map.RowArr[y-1];
    if(!Row)
    {
        Row=[];
        Map.RowArr[y-1]=Row;
    }
    Row[x-1]=Item;
}



"public"
function Transfer(Params)
{
    var Map=ReadMap(Params.ID);
    if(!Map)
        return;

    if(Params.Arr)
    for(var i=0;i<Params.Arr.length;i++)
    {
        var Item=Params.Arr[i];
        GetItemAt(Map,Item[0],Item[1],1);
    }
        
    var Count=0;
    for(var i=0;i<Map.TransferArr.length;i++)
    {
        var Item=Map.TransferArr[i];
        if(Item.Account && (Params.All==1 || context.FromNum==Item.Account))
        {
            Move(context.Smart.Account,Item.Account,Item.Amount,"Withdraw");
            Count++;
            if(Count>=Params.Count)
                break;
        }
        
        Map.TransferArr.splice(i,1);
        i--;
    }
    
    WriteMap(Map);
    
    RunEvent("Transfer",Params);
}


"public"
function Set(Params)
{
    if(!context.FromNum)
        throw "Error sender account";
    
    var Amount=Math.floor(Params.Amount);
    var ItemNum=Params.Item;
    var Direct=Params.Direct;
    var x=Params.x;
    var y=Params.y;

    var Map=ReadMap(Params.ID);
    if(!Map)
        return;
    
    var Num=context.FromNum;
    if(context.Smart.Num==7)
        Num=Num%17;
    var CountItems=1+Map.FinishItems-Map.StartItems;
    var NeedItemNum=Map.StartItems+Num%CountItems;
    
    //checks
    if((!Amount && Amount!=0) || Amount<Map.MinAmount || Amount>Map.MaxAmount)
        throw "Error Amount: "+Params.Amount;
    //if(!ItemNum || ItemNum<Map.StartItems || ItemNum>Map.FinishItems)
    if(ItemNum!=NeedItemNum)
        throw "Error Item: "+Params.Item;
    if(!Direct || Direct<1 || Direct>8)
        throw "Error Direct: "+Direct;
    if(!x || x<1 || x>Map.SizeX || !y || y<1 || y>Map.SizeY)
        throw "Error size x,y: "+x+","+y;
        
    if(Amount)
        Move(context.FromNum,context.Smart.Account,Amount,"Deposit");
    
    var Item=GetItemAt(Map,x,y,1);
    if(Item)
        throw "Error - cell not empty";
        
        
    
    var PosA1=GetDirectPos(x,y,Direct);
    var x2=PosA1.x;
    var y2=PosA1.y;
    var Item2=GetItemAt(Map,x2,y2,1);
    if(!Item2)
        throw "Unit not specified on x,y: "+x2+","+y2;
    
    var Egg=0,Fee=0,WinAmount=0;
    var Amount2=Item2.Amount;
    if(Amount2)
    {
        var ItemNum2=Item2.Item;
        if(ItemNum2==ItemNum)
        {
            throw "You can't attack a similar unit";
        }
        else
        //if(ItemNum2>=Map.StartItems && ItemNum2<=Map.FinishItems)
        {
            var Delta,APool,AReward;

            //если мы меньше
            if(Amount<Amount2 && Map.MinAmount!==0)
            {
                throw "Min amount = "+Amount2;
                //Delta=Amount/10;
            }
            else
            {
                //мы больше в два раза - берем все
                if(Amount>=Amount2*2 || Amount2<Map.MinAmount || Amount2<=1)
                    Delta=Amount2;
                else//берем половину
                    Delta=Amount2/2;
            }
            Delta=Math.floor(Delta);

            if(ItemNum2<Map.StartItems)//gift
            {
                AReward=Delta;
                APool=0;
            }
            else
            {
                AReward=Math.floor(Delta*Map.AReward/100);
                APool=Math.floor(Delta*Map.APool/100);
            }
            Fee=Delta-AReward-APool;

            Amount2-=Delta;
            Amount+=AReward;
            WinAmount=AReward;
            Map.AwardPool+=APool;
            Map.GameFee+=Fee;
                
            if(Amount2>0)
            {
                Item2.Amount=Amount2;
            }
            else
            {
                Item2.Amount=0;
                Item2.Item=1;//Egg
                Item2.BlockNum=context.BlockNum;
                Item2.TrNum=context.TrNum;
                Item2.Direct=0;
                
                if(ItemNum2<Map.StartItems)//gift
                    Map.AwardCount--;
                else
                if(Amount>0)//win 1
                {
                    Egg++;
                }
            }
            
            //!!!
            
            SetItemAt(Map,x2,y2,Item2);
            
            if(Amount<=0)
            {
                Amount=0;
                ItemNum=1;
                Direct=0;
            }
  
        }

        
    }

    SetItemAt(Map,x,y,{Account:context.FromNum, BlockNum:context.BlockNum,TrNum:context.TrNum, Amount:Amount, Egg:Egg, Fee:Fee, Item:ItemNum, Direct:Direct, Message:!!Params.Message,WinAmount:WinAmount});
    
    ClearDirects(Map,x,y);

    if(!SetAward(Map))
        WriteMap(Map);
    
    RunEvent("Set",Params);
}



function SetAward(Map)
{
    if(Map.AwardCount>=Map.AwardMaxCount)
        return;
    
    var Delta=context.BlockNum-Map.LastAwardTime;
    if(Delta<Map.AwardPeriod && Map.AwardCount>=Map.AwardMinCount)
        return;
    var Amount=Math.floor(Map.AwardPool/Map.AwardK);
    if(Amount<Map.MinAwardAmount)
        return;
    
    Amount = Math.min(Amount,Map.MaxAwardAmount)

    for(var i=0;i<5;i++)
    {
        //rnd    
        var x=1+context.BlockHash[10+i*2]%Map.SizeX;
        var y=1+context.BlockHash[10+i*2+1]%Map.SizeY;
        var Item=GetItemAt(Map,x,y,1)
        if(!Item)
        {
            Item={Account:0,BlockNum:context.BlockNum,TrNum:context.TrNum,Amount:Amount,Item:10,Direct:0};
            SetItemAt(Map,x,y,Item);
            
            Map.LastAwardTime=context.BlockNum;
            Map.AwardPool-=Amount;
            Map.AwardCount++;
            
            ClearDirects(Map,x,y);
            WriteMap(Map);
            return 1;
        }
    }
    return 0;
}

function ClearDirects(Map,x,y)
{
    for(var dy=-1;dy<=1;dy++)
    for(var dx=-1;dx<=1;dx++)
    {
        var y2=y+dy;
        var x2=x+dx;
        var Item2=GetItemAt(Map,x2,y2);
        if(Item2)
        {
            var Pos2=GetDirectPos(x2,y2,Item2.Direct);
            if(Pos2.x==x && Pos2.y==y)
            {
                //clear
                Item2.Direct=0;
            }
        }
    }
}


"public"
function AddAward(Params)
{
    var Map=ReadMap(Params.ID);
    if(!Map)
        return;
    
    if(SetAward(Map))
        RunEvent("AddAward",Params);
}


"public"
function SetMap(Params)
{
    CheckOwnerPermission();
    
    if(!Params.Reset)
    {
        var Map0=ReadMap(Params.ID);
        if(Map0)
        {
            Params.RowArr=Map0.RowArr;
            Params.TransferArr=Map0.TransferArr;
            Params.GameFee=Map0.GameFee;
            Params.AwardCount=Map0.AwardCount;
            Params.LastAwardTime=Map0.LastAwardTime;
        }
    }
    WriteMap(Params);
        

    RunEvent("SetMap",Params);
}

"public"
function GetMap(Params)
{
    return ReadMap(Params.ID);
}

//Proxy

function OnGet()//getting coins
{
    if(context.SmartMode)
    {
        var Item=context.Description;
        if(typeof Item==="object")
        {
            return CallProxy(Item.cmd,Item);
        }
    }
    else
    if(context.Description.substr(0,1)==="{")
    {
        if(SetState())
            return;
    }
}








//Lib
function CheckBaseCall()
{
    if(context.Account.Num!==context.Smart.Account)
        throw "Error call Account = "+context.Account.Num+"/"+context.Smart.Account;
}
function CheckOwnerPermission()
{
    if(context.FromNum!==context.Smart.Owner)
        throw "Access is only allowed from Owner account: "+context.FromNum+"/"+context.Smart.Owner;
}
function CheckSenderPermission()
{
    if(context.FromNum!==context.Account.Num)
        throw "Access is only allowed from Sender account: "+context.FromNum+"/"+context.Account.Num;
}

"public"
function GetKey(Params)
{
    return ReadValue(Params.Key,Params.Format);
}

"public"
function SetKey(Params)
{
    return WriteValue(Params.Key,Params.Value,Params.Format);
}


function SetState()
{
    if(context.Account.Num===context.Smart.Account
        && context.FromNum===context.Smart.Owner
        && context.Description.substr(0,1)==="{")
    {

        var State=ReadState(context.Smart.Account);
        var Data=JSON.parse(context.Description);
        for(var key in Data)
            State[key]=Data[key];

        WriteState(State);
        Event(State);
        return 1;
    }
    return 0;
}

function RunEvent(Name,Data)
{
    Event({"cmd":Name,Data:Data});
}

